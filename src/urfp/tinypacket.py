#!usr/bin/python3
""" Module for transmitting and receiving URFP tinypackets over UDP etc. """

import socket
import select
import time
import struct
import asyncio
import logging
import threading

from urfp.argtype import URFPArgType

class URFPTinypacketProtocol:
    """ asyncio.Protocol implementation to receive URFP Tinypackets """
    def __init__(self, handler=None, srcdata=None): #pylint: disable=unused-argument
        self._logger = logging.getLogger(__name__)
        self._handler = handler
        self._transport = None

    def connection_made(self, transport):
        """ asyncio.Protocol connection_made callback """
        self._transport = transport

    def error_received(self, exc):
        """ asyncio.Protocol error_received callback """
        self._logger.warning('Error received: %s', exc)

    def connection_lost(self, exc):
        """ asyncio.Protocol connection close callback """
        self._logger.debug('Connection closed: %s', exc)

    def data_received(self, data):
        """ Decode data from stream """

    def datagram_received(self, data, addr):
        """ Decode data from packet """
        if self._handler:
            self._handler.input(data)

class URFPDeserializeCompound:
    """ Used for deserializing arrays etc. """

    def __init__(self, index=0, length=0, data=None):
        self.index  = index
        self.length = length
        self.data   = data

class URFPTinypacketSerDesEnv:
    """Buffer with preallocated memory"""

    def __init__(self, code, input_alloc_size):
        self.code = code
        self.final = False
        self.input_alloc_size = input_alloc_size
        self.input = bytearray(input_alloc_size)
        self.input_maxlen = input_alloc_size
        self.input_len = 0
        self.output = bytearray()
        self.expected = 0
        self.age = 0

    def extend_input(self, data, offset, length):
        """ Append more data to input, eventually resize the preallocated buffer """
        new_len = self.input_len + length
        if new_len > self.input_maxlen:
            self.input = self.input + bytearray(self.input_alloc_size)
            self.input_maxlen = self.input_maxlen + self.input_alloc_size
        self.input[self.input_len:new_len] = data[offset:(offset+length)]
        self.input_len = new_len

class URFPTinypacketCodec:
    """ An URFPTinypacketCodec can build or parse Tinypackets to form requests or
        understand responses. It can be used by clients or servers and other tools
        like protocol sniffers. """

    _TINYPACKET_SIZE = 8
    # Only 8 Byte fit in CAN messages. Length encoding in header sets another limit of
    # 32 byte payload + 4 byte header = 36 byte max tinypacket size
    _FRAME_LEN_LIMIT = 1024
    # Current smallest known receiver buffer: 1024 bytes for whole Ethernet frame with preamble

    _TAG_MAX = 128

    _HEADER_STRUCT = struct.Struct('<II')
    _HEADER_LEN = struct.calcsize('<II')

    # _env{tag}:
    #   code: String, eg. 'lg' or 'gv', without trailing tag
    #   final: True if final packet was received. In that case, 'expected' carries the
    #          final prefix (8 ... 15)
    #   input: Concatenated input up to now
    #   output: Concatenated output (remains empty during input)
    #   expected: prefix expected in next packet, if not yet final
    #   age: For garbage collection. Zeroed when an update to this _env is received,
    #        incremented if other packets come in.

    def __init__(self):
        self._logger = logging.getLogger(__name__)
        self._tag = ord('0') # ord() of most recently used tag
        self._env = [None] * self._TAG_MAX
        self._discarded = None
        self._source_type = { }
        self._subscriber = { }

        self._arg_struct = { }
        self._arg_struct[URFPArgType.URFP_INT8]   = struct.Struct('b')
        self._arg_struct[URFPArgType.URFP_INT16]  = struct.Struct('<h')
        self._arg_struct[URFPArgType.URFP_INT32]  = struct.Struct('<i')
        self._arg_struct[URFPArgType.URFP_INT64]  = struct.Struct('<q')
        self._arg_struct[URFPArgType.URFP_UINT8]  = struct.Struct('B')
        self._arg_struct[URFPArgType.URFP_UINT16] = struct.Struct('<H')
        self._arg_struct[URFPArgType.URFP_UINT32] = struct.Struct('<I')
        self._arg_struct[URFPArgType.URFP_UINT64] = struct.Struct('<Q')
        self._arg_struct[URFPArgType.URFP_FLOAT]  = struct.Struct('<f')
        self._arg_struct[URFPArgType.URFP_DOUBLE] = struct.Struct('<d')

    def _str_typedesc(self, typedesc):
        """ Return a list of strings describing the complex type """
        return [str(x) for x in typedesc]

    def _serialize(self, arg, argtype, type_index=0):
        """ Recursively serialize data from dict into bytearray according to the URFPArgTypes
        listed in argtype. These describe scalar types and borders (start and end) of arrays
        and structs. Beside the bytearray resulting from serializing, this returns the index
        into argtype list where serializing should continue"""

        code = argtype[type_index].code

        if code == URFPArgType.URFP_ARRAY_START:
            #print('ARRAY ARG', arg)
            result = bytearray()
            nitems = argtype[type_index].length
            type_index = type_index + 1 # point to data type of array elements
            o = type_index + 1 # point beyond data type ie. to array end
            for v in arg:
                b, o = self._serialize(v, argtype, type_index)
                result.extend(b)
            if argtype[o].code != URFPArgType.URFP_ARRAY_END:
                # Workaround if server explicitly repeated type info for EACH array element,
                # not just one exemplary one
                potential_end = type_index + (o-type_index) * nitems
                if potential_end <= len(argtype) \
                    and argtype[potential_end].code == URFPArgType.URFP_ARRAY_END:

                    o = potential_end
                else:
                    raise TypeError("Typeinfo ARRAY not ending with ARRAY_END")
            return result, o + 1

        if code == URFPArgType.URFP_STRUCT_START:
            #print('STRUCT ARG', arg)
            result = bytearray()
            type_index = type_index + 1
            while type_index < len(argtype) \
                and argtype[type_index].code != URFPArgType.URFP_STRUCT_END:

                b, type_index = self._serialize(arg[argtype[type_index].name], argtype, type_index)
                result.extend(b)
            if argtype[type_index].code != URFPArgType.URFP_STRUCT_END:
                raise TypeError("Typeinfo STRUCT not ending with STRUCT_END")
            return result, type_index + 1

        if code == URFPArgType.URFP_BOOL:
            #print('BOOL ARG', arg)
            if arg:
                return b'\x01', type_index + 1
            return b'\x00', type_index + 1

        if code == URFPArgType.URFP_STRING:
            #print('STRING ARG', arg)
            return arg.encode('utf-8') + b'\0', type_index + 1

        if ((URFPArgType.URFP_INT <= code < URFPArgType.URFP_INT+0x08) or \
                (code == URFPArgType.URFP_FLOAT) or (code == URFPArgType.URFP_DOUBLE)):
            #print('INT ARG', arg)
            return self._arg_struct[code].pack(arg), type_index + 1

        raise TypeError("strange type 0x{argtype[type_index].code:02x}")

    def serialize(self, arg, argtype):
        """ Call the recursive _serialize() starting at type_index==0 """
        (result, _) = self._serialize(arg, argtype, 0)
        return result

    def deserialize(self, r, offset, resulttype):
        """ Deserialize binary data from r according to the URFPArgTypes listed in
        resulttype. These describe scalar types and borders (start and end) of arrays
        and structs. A "compound" stack is used to track in which sub-element of a
        nested arrangement the deserializer should put the next element into.
        ATM, this deserializes a bytestream until it is exhausted. TODO:
        Memorize deserializer state at end of bytestream so it can be later continued
        with more byte data from continuation packets. """

        start_index = offset # index where processing started
        res_index = offset   # index of next byte to process from r
        type_index = 0  # index of next URFPArgType to process from resulttype

        result = {}     # the resulting structured data (return value of this function)

        # "compound" is a stack in which the latest element[-1] describes the current
        # structure (a dict or list) in which new scalars should be appended. Each
        # element in "compound" describes
        #   index - the index of the element starting the structure in resulttype list,
        #   length - the array length (-1 if indefinite until all input is exhausted),
        #   data - the actual data structure building up (for appending new values).

        # Whenever the deserializer reaches a URFP_.._END in the resulttype list,
        # it pops the latest element from compound list and appends the resulting
        # data structure as a "value" to the outer compound (or result, if
        # there is no outer left).

        compound = []

        while type_index < len(resulttype):

            next_type_index = type_index + 1

            # Either a compound START

            if resulttype[type_index].code == URFPArgType.URFP_ARRAY_START:
                compound.append(URFPDeserializeCompound(index=type_index,
                    length=resulttype[type_index].length, data = [] ))

            elif resulttype[type_index].code == URFPArgType.URFP_STRUCT_START:
                compound.append(URFPDeserializeCompound(index=type_index, data = {} ))

            else:
                value = None
                name = resulttype[type_index].name

                # ... Or a compound END.

                code = resulttype[type_index].code
                if code == URFPArgType.URFP_STRUCT_END:

                    if len(compound) == 0:
                        raise TypeError("Typeinfo with STRUCT_END ending nothing")

                    if resulttype[compound[-1].index].code != URFPArgType.URFP_STRUCT_START:
                        raise TypeError("Typeinfo with STRUCT_END not ending STRUCT")

                    name = resulttype[compound[-1].index].name
                    value = compound.pop().data

                # If this is an ARRAY end and it has not been filled up to its length yet,
                # continue reading types back at ARRAY START

                elif code == URFPArgType.URFP_ARRAY_END:
                    if len(compound) == 0:
                        raise TypeError("Typeinfo with STRUCT_END ending nothing")

                    if resulttype[compound[-1].index].code != URFPArgType.URFP_ARRAY_START:
                        raise TypeError("Typeinfo with ARRAY_END not ending ARRAY")

                    remaining = compound[-1].length # remaining length
                    if remaining == -1 and res_index < len(r):
                        # unspecified length and more data to parse available in input
                        next_type_index = compound[-1].index + 1 # back to first type in array
                    else:
                        # if length is specified (not -1), this means the (potentially very
                        # long) other type description also includes this many elements (each
                        # single element) and now at the URFP_ARRAY_END
                        # we're done with whole array

                        name = resulttype[compound[-1].index].name
                        value = compound.pop().data

                else: # ... Or a scalar value to append to result/compound.
                    try:
                        if code == URFPArgType.URFP_BOOL:

                            value = r[res_index] != 0
                            res_index = res_index + 1

                        elif code == URFPArgType.URFP_STRING:

                            string_len = r.find(b'\0', res_index) - res_index
                            string_bytes = r[res_index:(res_index+string_len)]
                            value = string_bytes.decode('utf-8')
                            res_index = res_index + string_len + 1

                        elif ((URFPArgType.URFP_INT <= code < URFPArgType.URFP_INT+0x08) or
                            (code == URFPArgType.URFP_FLOAT) or (code == URFPArgType.URFP_DOUBLE)):

                            num_bytes = self._arg_struct[code].size
                            value, = self._arg_struct[code].unpack_from(r, offset=res_index)
                            res_index = res_index + num_bytes

                        elif code == URFPArgType.URFP_TYPE_INFO:

                            value = r[res_index:] # all the remainder
                            res_index = len(r)

                        else:
                            raise TypeError("strange type 0x{resulttype[type_index].code:02x}")

                    except Exception as exception:
                        alltypes = ''
                        for t in resulttype:
                            alltypes = alltypes + ' ' + str(t)
                        self._logger.info('ex resulttype: %s, type_index=%d, '
                            'res_index=%d, length=%d, remainder=%s', alltypes,
                            type_index, res_index, len(r), str(r[res_index:]))
                        raise exception

                if value is not None:
                    if len(compound) > 0:
                        if resulttype[compound[-1].index].code == URFPArgType.URFP_ARRAY_START:
                            compound[-1].data.append(value)
                        else: # must be struct
                            compound[-1].data[resulttype[type_index].name] = value
                    else:
                        result[name] = value

            type_index = next_type_index

        return result, res_index-start_index

    def input(self, data):
        """Feed content of UDP packet to URFP Tinypacket Frame instance.
        It will be deconstructed into tinypackets which are then collected
        into buffers separated by their code and tag. The result is a list
        of tags for which buffers have been updated."""

        updated = { }
        offset = 0
        max_offset = len(data) - self._TINYPACKET_SIZE
        while offset <= max_offset:

            prefix = (data[offset] & 0xf0) >> 4
            tag = data[offset+1] & 0x7f
            env = None

            length = data[offset] & 0x0f
            #if (data[offset+1] & 0x80) != 0:
            #    length = length + 16    # TBD: This can happen only with _TINYPACKET_SIZE >= 20

            if prefix >= 12: # initial packet with code and 4 bytes payload

                code = chr(0x60 + (data[offset+2] & 0x1f)) + chr(0x60 + (data[offset+3] & 0x1f))

                env = URFPTinypacketSerDesEnv(code, self._FRAME_LEN_LIMIT)
                self._env[tag] = env
                updated[tag] = True

                if length > 0:
                    self._env[tag].extend_input(data, offset+4, length)

                if prefix < 15:
                    env.final = True
                    env.expected = prefix

            else: # prefix < 12: continuation packet, 6 bytes payload

                good = False
                if self._env[tag] is not None:
                    env = self._env[tag]
                    expected = env.expected
                    if prefix >= 8 or prefix == expected:
                        if prefix >= 8:
                            env.final = True
                            env.expected = prefix
                        else:
                            env.expected = (expected + 1) % 8
                        if length > 0:
                            env.extend_input(data, offset+2, length)
                        updated[tag] = True
                        env.age = 0
                        self._discarded = None
                        good = True

                if not good:
                    if tag != self._discarded:
                        self._logger.info("Discarded continuation data for unknown tag '%c'", tag)
                        self._discarded = tag

            offset = offset + self._TINYPACKET_SIZE

        # Take care of OOB input (Code 'lg') directly by passing it to
        # subscriber.input() or (deprecated) .oob_input(), but only IF
        # there are any subscribers (so data would be passed untouched when
        # Codec is called by the utdis network tracing tool).

        # This only done AFTER processing ALL tinypackets in a frame, assuming that
        # no single frame can contain more than one transaction (packets belonging to
        # one same tag)

        if len(self._subscriber) > 0:
            for tag in updated:
                env = self._env[tag]
                if env.final and env.code == 'lg':
                    if env.input_len >= self._HEADER_LEN:
                        source_id, first_index = self._HEADER_STRUCT.unpack_from(env.input)
                        if source_id in self._subscriber:
                            deprecated_fn = getattr(self._subscriber[source_id], "oob_input", None)
                            if deprecated_fn is not None and callable(deprecated_fn):
                                off = self._HEADER_LEN
                                while off < env.input_len:
                                    source_data, sz = self.deserialize(env.input,
                                        off, self._source_type[source_id])
                                    self._subscriber[source_id].oob_input(source_id,
                                        first_index, source_data)
                                    first_index = first_index + 1
                                    off = off + sz
                            else:
                                raw_data = env.input[self._HEADER_LEN:env.input_len]
                                self._subscriber[source_id].input(source_id, first_index, raw_data)

                    self._env[tag] = None
                    updated[tag] = False

        # keep only the fresh ones
        for i in range(self._TAG_MAX):
            if self._env[i] is not None and self._env[i].age > 100:
                self._env[i] = None

        return updated

    def get(self, tag, clean=True):
        """ Get the payload received within the environment identified by tag. """
        if self._env[tag] is not None:
            data = self._env[tag].input[:self._env[tag].input_len]
            if clean:
                self._env[tag] = None
            return data

        return None

    def _next_tag(self, curr_tag):
        """ Compute a new tag that wasn't used for some time. """
        next_tag = curr_tag + 1
        if next_tag > ord('9'):
            next_tag = ord('0')
        return next_tag

    def start_request(self, code, args=None, argtype=None):
        """ Allocate a new URFPTinypacketSerDesEnv to start with. """
        tag = self._next_tag(self._tag)
        self._env[tag] = URFPTinypacketSerDesEnv(code, self._FRAME_LEN_LIMIT)
        self._tag = tag
        return tag

    def generate_frames(self, tag, args=None, argtype=None):
        """ This method generates the frames that constitute the request.
        'code' is the two-letter code without the tag.
        The tag is chosen and added by this method as appropriate. The args should be a dict
        of data to transmit with the request. They will be serialized in the order in which
        they appear in the dict (starting with Python 3.7) and must match whatever the server
        expects regarding order, type and size. The serializer is work in progress, see serialize()
        """
        if args:
            self._env[tag].output.extend(self.serialize(args, argtype))

        # Current frame code in rotor expects at least one separate final packet, so we
        # do not make use of the single init+final packet option with payload up to 4 bytes yet
        if False: # len(self._env[tag].output) <= 4: #pylint: disable=using-constant-test

            prefix = 12 # just one packet, initial and final at the same time.
            length = len(self._env[tag].output)
            code = self._env[tag].code
            packet = bytearray(((prefix<<4)+length, tag, ord(code[0]), ord(code[1])))
            if length > 0:
                packet.extend(self._env[tag].output[0:length])
            while len(packet)<8:
                packet.append(0) # pad with zeroes
            frame = packet

        else:
            prefix = 15 # initial packet, more will follow

            frame = bytearray()

            length = min(4, len(self._env[tag].output))
            offset = length
            code = self._env[tag].code
            packet = bytearray(((prefix<<4)+length, tag, ord(code[0]), ord(code[1])))
            if length > 0:
                packet.extend(self._env[tag].output[0:length])
            while len(packet)<8:
                packet.append(0) # pad with zeroes
            frame.extend(packet)

            prefix = 0
            while prefix < 8:
                length = len(self._env[tag].output) - offset
                if length > 6:
                    length = 6
                else:
                    prefix = 8
                packet = bytearray(((prefix<<4)+length, tag))
                packet.extend(self._env[tag].output[offset:(offset+length)])
                offset += length
                while len(packet)<8:
                    packet.append(0) # pad with zeroes
                frame.extend(packet)

                if len(frame) > self._FRAME_LEN_LIMIT - self._TINYPACKET_SIZE:
                    # Send data before frame size exceeds limit
                    self._logger.debug('send %s', str(frame))

                    yield frame # Our caller here should use sendto to output frame via network

                    frame = bytearray()

        # Send remaining data
        if len(frame) > 0:
            yield frame # Our caller here should use sendto to output frame via network

        self._env[tag].final = False

    def get_source_type(self, source_id):
        """ Return the type information for the source with given ID """
        return self._source_type[source_id]

    def subscribe(self, source_id, resulttype, handler=None):
        """ Store type info and handler class instance to handle incoming
        OOB data from a given source ID. """
        self._source_type[source_id] = resulttype
        self._subscriber[source_id] = handler

    def unsubscribe(self, source_id):
        """ Remove type info and handler class instance. """
        if source_id in self._source_type:
            del self._source_type[source_id]
            del self._subscriber[source_id]


class URFPTinypacketFrame:
    """ This class provides methods to construct and interpret URFP tinypackets
    inside frames (e.g. UDP payload). An instance can be used to make requests
    to an URFP server and handle responses to the requests as well as collect
    incoming OOB data."""

    def __init__(self, server='10.0.10.62:6272', local='10.255.255.255:6272'):
        """Constructor. The constructor might be called without an argument
        or with a single argument. If no argument is given, a default server
        address and port is assumed ('10.0.10.62:6272'). A given argument is
        used to set the server address explicitely."""
        self._logger = logging.getLogger(__name__)

        (self._server_ip, self._server_port) = server.split(':')
        if self._server_port:
            self._server_port = int(self._server_port)
        else:
            self._server_port = 6272
        (self._local_ip, self._local_port) = local.split(':')
        if self._local_port:
            self._local_port = int(self._local_port)
        else:
            self._local_port = 0

        self._codec = URFPTinypacketCodec()

        self._socket = None
        self._listener = None
        self._listener_run = None
        self._transport = None
        self._protocol = None
        #raise URFPException(json["error_code"], json["error_text"])
        #self._logger(url, query_string, http_status, http_content)

    async def _async_start_udp_receiver(self):
        """ Start a listener for UDP packets. """
        loop = asyncio.get_running_loop()

        if not self._socket:
            self._socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
            self._socket.bind(("", self._local_port))
            # TBD s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            #self._socket.bind(socket.INADDR_BROADCAST, 6272)
            self._socket.setblocking(0)

        self._transport, self._protocol = await loop.create_datagram_endpoint(
            lambda: URFPTinypacketProtocol(handler=self._codec), sock=self._socket)
        # Socket is now owned by transport
        self._socket = None

    async def async_start_receiver(self):
        """ Start a listener for datagrams (async version) """
        await self._async_start_udp_receiver()

    async def async_stop_receiver(self):
        """ Stop the listener and cleanup ressources (async version) """
        self._transport.close()
        self._transport = None

    async def async_request(self, code, args=None, argtype=None, resulttype=None):
        """ async version of perform_request """

        if self._transport is None:
            raise NotImplementedError("Sending async requests without async receiver started " \
                "is not implemented yet, please start receiver first")

        tag = self._codec.start_request(code)

        # Generate and send the request as UDP frames
        for frame in self._codec.generate_frames(tag, args=args, argtype=argtype):
            self._logger.debug('send %s%c %d', self._codec._env[tag].code, tag, len(frame))
            self._transport.sendto(frame, (self._server_ip, int(self._server_port)))

        # Receive response frames until timeout or final Tinypacket has been recognized
        start = time.time()
        timeout = start + 1
        while time.time() < timeout and not self._codec._env[tag].final:
            await asyncio.sleep(0.1)

        return self._finish_request(tag, resulttype)

    def _udp_listener(self):
        """ Core loop for the UDP listener thread """
        if not self._socket:
            self._socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
            self._socket.bind(("", self._local_port))
            # TBD s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            #self._socket.bind(socket.INADDR_BROADCAST, 6272)
            self._socket.setblocking(0)

        # Discard possibly already queued frames from server beforehand
        # to maximize the chance of getting the answer to our request
        ready = select.select([self._socket], [], [], 0)
        while ready[0]:
            data, _ = self._socket.recvfrom(1536)
            ready = select.select([self._socket], [], [], 0)

        while self._listener_run:
            ready = select.select([self._socket], [], [], 0.1)
            if ready[0]:
                data, _ = self._socket.recvfrom(1536)
                # todo: ensure input order (UDP doesn't guarantee order)
                # FIXME updates is not used
                updates = self._codec.input(data)

    def _start_udp_receiver(self):
        """ Start an UDP listener for OOB data """
        if self._listener is not None:
            raise ConnectionError('ERROR: listener is already running')

        self._listener_run = True
        self._listener = threading.Thread(target=self._udp_listener)
        self._listener.start()

    def start_receiver(self):
        """ Start a listener for OOB data """
        self._start_udp_receiver()

    def stop_receiver(self):
        """ Stop the listener thread and clean up """
        if self._listener is not None:
            self._listener_run = False
            self._listener.join()
            self._listener = None

    def perform_request(self, code, args=None, argtype=None, resulttype=None):
        """ This method performs the request. 'code' is the two-letter code without the tag.
        The tag is chosen and added by this method as appropriate. The args should be a dict
        of data to transmit with the request. They will be serialized in the order in which
        they appear in the dict (starting with Python 3.7) and must match whatever the server
        expects regarding order, type and size. The serializer is work in progress, see serialize()
        """

        #self._logger.debug('argtype %s', str(self._codec._str_typedesc(argtype)))
        #self._logger.debug('resulttype %s', str(self._codec._str_typedesc(resulttype)))

        if self._transport is not None:
            raise NotImplementedError("Sending sync requests while receiving data " \
                "is not implemented yet, please stop receiver first")

        tag = self._codec.start_request(code)

        if not self._socket:
            self._socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
            self._socket.bind(("", self._local_port))
            # TBD s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            #self._socket.bind(socket.INADDR_BROADCAST, 6272)
            self._socket.setblocking(0)

        if not self._listener:
            # Discard possibly already queued frames from server beforehand
            # to maximize the chance of getting the answer to our request
            ready = select.select([self._socket], [], [], 0)
            while ready[0]:
                data, _ = self._socket.recvfrom(1536)
                ready = select.select([self._socket], [], [], 0)

        # Generate and send the request as UDP frames
        for frame in self._codec.generate_frames(tag, args=args, argtype=argtype):
            self._logger.debug('send %s%c %d', self._codec._env[tag].code, tag, len(frame))
            self._socket.sendto(frame, (self._server_ip, int(self._server_port)))

        # Receive response frames until timeout or final Tinypacket has been recognized
        start = time.time()
        timeout = start + 0.5
        if self._listener:
            while time.time() < timeout and not self._codec._env[tag].final:
                # todo: better signalling
                time.sleep(0.01)
        else:
            while time.time() < timeout and not self._codec._env[tag].final:
                ready = select.select([self._socket], [], [], 0.1)
                if ready[0]:
                    data, _ = self._socket.recvfrom(1536)
                    # todo: ensure input order (UDP doesn't guarantee order)
                    # FIXME updates is not used
                    updates = self._codec.input(data)

        return self._finish_request(tag, resulttype)

    def _finish_request(self, tag, resulttype=None):

        env = self._codec._env[tag]

        # TBD: Use own exceptions
        if not env.final:
            if env.input_len > 0:
                raise TimeoutError(f'Incomplete response to "{env.code}{tag:c}"')
            else:
                raise TimeoutError(f'No response to "{env.code}{tag:c}"')

        final_prefix = env.expected

        if final_prefix in (9,13):
            raise ConnectionError('ERROR: got URFP_ERR (final_prefix is 9 or 13, ' \
                'retrying is useless)')
        if final_prefix in (10,14):
            raise RuntimeWarning('ERROR: got URFP_AGAIN (final_prefix is 10 or 14, ' \
                'please retry)')

        # Get all the raw binary data response
        r = self._codec.get(tag, True)

        if not resulttype:
            self._logger.debug('no resulttype')
            return None

        if not r:
            self._logger.debug('no r')
            return None

        # Parse the binary data according to the resulttype spec
        result, rem_off = self._codec.deserialize(r, 0, resulttype)
        if result is None:
            self._logger.debug('deserialize None')
        if rem_off < len(r):
            self._logger.debug('more data after deserialized: %s', str(r[rem_off:]))
        return result


    def _prepare_list_request(self, listname, parameters):

        ## Due to a bug in urfp_server < 16639, pattern shouldn't be longer than 7 chars
        if 'pattern' in parameters.keys():
            parameters['pattern'] = parameters['pattern'][:7]

        argtype = [
            URFPArgType('args',   -1, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('pattern', 0, type_byte = URFPArgType.URFP_STRING),
            URFPArgType('',        0, type_byte = URFPArgType.URFP_STRUCT_END)]

        resulttype = [
            URFPArgType(listname, -1, type_byte = URFPArgType.URFP_ARRAY_START),
              URFPArgType('elem',    0, type_byte = URFPArgType.URFP_STRUCT_START),
                URFPArgType('id',      0, type_byte = URFPArgType.URFP_UINT32),
                URFPArgType('name',    0, type_byte = URFPArgType.URFP_STRING),
              URFPArgType('',        0, type_byte = URFPArgType.URFP_STRUCT_END),
            URFPArgType('',        0, type_byte = URFPArgType.URFP_ARRAY_END)]

        return parameters, argtype, resulttype

    def perform_list_request(self, cmd, listname, parameters):
        """ retrieve array of {id:i32, name:string} structs """

        parameters, argtype, resulttype = self._prepare_list_request(listname, parameters)
        return self.perform_request(cmd, parameters, argtype, resulttype)

    async def async_list_request(self, cmd, listname, parameters):
        """ Async version of perform_help_request """

        parameters, argtype, resulttype = self._prepare_list_request(listname, parameters)
        return await self.async_request(cmd, parameters, argtype, resulttype)

    def _prepare_help_request(self, elemtype, elemid):

        parameters = { 'id': elemid }

        argtype = [
            URFPArgType('args',   -1, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('id',      0, type_byte = URFPArgType.URFP_INT32),
            URFPArgType('',        0, type_byte = URFPArgType.URFP_STRUCT_END)]

        resulttype = [
            URFPArgType('desc',    0, type_byte = URFPArgType.URFP_STRING),
            URFPArgType('type',    0, type_byte = URFPArgType.URFP_TYPE_INFO)]

        return parameters, argtype, resulttype

    def perform_help_request(self, elemtype, elemid):
        """ retrieve help, ie. description string and arg/return type info.
        Note: The type info (in retval['type']) is in a format that is understood
        by perform_request of this class. It is returned to upper layers
        for later use with this class but not with others and thus should be
        regarded 'obstructed' """

        parameters, argtype, resulttype = self._prepare_help_request(elemtype, elemid)
        r = self.perform_request('h'+elemtype, parameters, argtype, resulttype)
        return self._finish_help_request(elemid, r)

    async def async_help_request(self, elemtype, elemid):
        """ Async version of perform_help_request """

        parameters, argtype, resulttype = self._prepare_help_request(elemtype, elemid)
        r = await self.async_request('h'+elemtype, parameters, argtype, resulttype)
        return self._finish_help_request(elemid, r)

    def _finish_help_request(self, elemid, r):

        help_dict = { 'id': elemid, 'desc': r['desc'], 'type': {}, 'args': {} }

        r = r['type']
        typeindex = 0
        typecount = 0
        uargs = []
        utype = []

        while typeindex+1 < len(r):
            typecount = typecount + 1
            type_prefix = r[typeindex]
            type_byte = r[typeindex+1]
            typeindex = typeindex + 2
            item_len = 0

            if type_byte == URFPArgType.URFP_ARRAY_START:
                item_len, = struct.unpack('<i', r[typeindex:typeindex+4])
                typeindex = typeindex + 4

            if (type_prefix & URFPArgType.URFP_NAMED) != 0:
                name_len = r[typeindex:].find(b'\0')
                item_name = r[typeindex:typeindex+name_len].decode('utf-8')
                typeindex = typeindex + name_len + 1 # past terminating NUL
            else:
                # build type description as from urfp_http
                item_name = f'r{typecount}'

            if (type_prefix & URFPArgType.URFP_ARG) != 0:
                uargs.append(URFPArgType(item_name, type_byte=type_byte, length=item_len))
            else:
                utype.append(URFPArgType(item_name, type_byte=type_byte, length=item_len))

        help_dict['args'] = uargs
        help_dict['type'] = utype
        self._logger.debug(help_dict)
        for t in help_dict['type']:
            self._logger.debug(str(t))
        return help_dict

    def subscribe(self, source_id, resulttype, handler):
        """ Store type info and handler class instance to handle incoming
        OOB data from a given source ID. """
        self._codec.subscribe(source_id, resulttype, handler)

    def unsubscribe(self, source_id):
        """ Remove type info and handler class instance. """
        self._codec.unsubscribe(source_id)

    def source_data_as_dict(self, source_id, input_data):
        """ Return a single source data item from input """
        result, _ = self._codec.deserialize(input_data, 0, self._codec.get_source_type(source_id))
        return result

    def source_data_as_list_of_dicts(self, source_id, input_data):
        """ Return a source data items from input as list (as many as there are) """
        off = 0
        result = []
        while off < len(input_data):
            source_dict, sz = self._codec.deserialize(input_data, off,
                self._codec.get_source_type(source_id))
            result.append(source_dict)
            off = off + sz
        return result



