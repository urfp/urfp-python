""" Peer for talking to URFP console server """

import re
import time
import logging
import serial

class URFPConsoleChatter:
    """ This class provides methods to talk to a URFP Console server """

    def __init__(self, device="/dev/ttyS0", logger=None):
        """Constructor """
        self._logger = (logger if logger else logging.getLogger(__name__))
        self._serial = serial.Serial(device, 115200, timeout=1)
        self._tag = 64 # ord() of most recently used tag
        self._env = { }

    def __exit__(self, type_, value, traceback):
        self._serial.close()

    def _send(self,q):
        self._logger.debug('TX:%s', str(q))
        for c in q:
            self._serial.write([c])
            time.sleep(0.01)
        self._serial.write(b'\r')
        time.sleep(0.01)

    def _recv(self):
        r = self._serial.readline()
        if r:
            self._logger.debug('RX:%s', str(r))
        else:
            self._logger.debug('-nothing-')
        return r

    ##==--

    def _next_tag(self, curr_tag):
        next_tag = curr_tag + 1
        if next_tag > ord('Z'):
            next_tag = ord('A')
        return next_tag

    def perform_request(self, code, *args):
        """ This method performs the request. 'code' is the two-letter code without the tag.
        The tag is chosen and added by this method as appropriate. The args should be a dict
        of data to transmit with the request. They will be serialized in the order in which
        they appear in the dict (starting with Python 3.7) and must match whatever the server
        expects regarding order, type and size. The serializer is work in progress, see serialize()
        """

        tag = None
        new_tag = self._next_tag(self._tag)
        while not tag and new_tag != self._tag:
            if new_tag not in self._env:
                tag = new_tag # found an unused tag
            else:
                new_tag = self._next_tag(new_tag)

        if not tag:
            self._logger.info("Didn't find an unique new tag for request")
            return None

        self._env[tag] = { 'code':code, 'final': False, 'input': r'' }

        output = code + chr(tag)

        if len(args) > 0:
            for arg in args[0]:
                output = output + b' ' + bytes(args[0][arg])

        # Transmit, with optional small delay between bytes (slow UART in device)

        self._send(b'')
        self._recv() # synced

        self._send(bytes(output, encoding='ascii'))
        self._recv() # ignore echo

        # Receive. Todo: Check TAG

        retval = None
        final = r'\*' + code + '.'
        line = self._recv()
        while line:
            line = line.decode('utf-8')
            # Split line at whitespaces; keep strings enclosed with "" and remove the quotes
            matches = re.findall(r'"(.*?)"|([^\s]+)', line)
            words = [m[0] if m[0]!='' else m[1] for m in matches]
            if re.match(final, words[0]):
                return retval
            # Return words in a list of lists
            if len(words) > 0:
                if retval is None:
                    retval = []
                retval.append(words[1:-1])
            line = self._recv()
        return retval

    def perform_list_request(self, cmd, listname, params):
        """ retrieve array[l of (i32,string) pairs AKA (id,name) """
        results:list[list[str,str]] = self.perform_request(cmd, params)
        # Return as a JSON-like dictionary
        json_results = {listname: [{"id": int(items[0]), "name": items[1]} for items in results]}
        return json_results

    def perform_help_request(self, elemtype, elemid):
        """ Ask for details about element """
        json_response = self.perform_request('h'+elemtype, {'id':str(elemid)})
        help_dict = { 'desc': json_response['desc'], 'type': {}, 'args': {} }
        for k in json_response:
            if k != 'desc':
                help_dict['type'][k] = json_response[k]
