""" Argument type description class for use in tinypacket """

class URFPArgType:
    """ This class is used to describe the data structure of one variable, function
    return value, function arguments, or source content. It is especially needed when
    receiving data from simple devices that cannot efficiently augment all data
    with field names and type info, e.g. in URFPTinypacket """

    # Binary representation as in Tinypacket
    URFP_TYPE_INFO    = 0x00

    # Type byte
    URFP_STRUCT_START = 0x40
    URFP_ARRAY_START  = 0x41 # followed by int32 array size (-1 or > 0)
    URFP_ARRAY_END    = 0x42
    URFP_STRUCT_END   = 0x43
    URFP_BOOL         = 0x44
    URFP_STRING       = 0x45
    URFP_FLOAT        = 0x46
    URFP_DOUBLE       = 0x47
    URFP_INT          = 0x48 # to be completed with ld2(wordlen) in LSB
    URFP_INT8         = 0x48 # = URFP_INT | ld2(1) = URFP_INT
    URFP_INT16        = 0x49 # = URFP_INT | ld2(2)
    URFP_INT32        = 0x4A # = URFP_INT | ld2(4)
    URFP_INT64        = 0x4B # = URFP_INT | ld2(8)
    URFP_UNSIGNED     = 0x4C # to be completed with ld2(wordlen) in LSB
    URFP_UINT8        = 0x4C # = URFP_UNSIGNED | ld2(1) = URFP_UNSIGNED
    URFP_UINT16       = 0x4D # = URFP_UNSIGNED | ld2(2)
    URFP_UINT32       = 0x4E # = URFP_UNSIGNED | ld2(4)
    URFP_UINT64       = 0x4F # = URFP_UNSIGNED | ld2(8)

    # 0x50..0xFF reserved for custom types

    # Type mode
    URFP_ARG          = 0x01 # Indicates that type information describes an argument not result
    URFP_NAMED        = 0x02 # Indicates that a name string follows type information

    def __init__(self, name, length=0, type_byte=None, type_str=None):
        self.name = name
        self.length = length
        if type_str:
            if type_str == '{':
                self.code = URFPArgType.URFP_STRUCT_START
            if type_str[0] == '[':
                self.code = URFPArgType.URFP_ARRAY_START
                if length==0:
                    if len(type_str)>0:
                        self.length = int(type_str[1:])
                    else:
                        self.length = -1
            if type_str == ']':
                self.code = URFPArgType.URFP_ARRAY_END
            if type_str == '}':
                self.code = URFPArgType.URFP_STRUCT_END
            if type_str == 'bool':
                self.code = URFPArgType.URFP_BOOL
            elif type_str == 'string':
                self.code = URFPArgType.URFP_STRING
            elif type_str == 'float':
                self.code = URFPArgType.URFP_FLOAT
            elif type_str == 'double':
                self.code = URFPArgType.URFP_DOUBLE
            elif type_str[0] == 'i':
                self.code = URFPArgType.URFP_INT
            elif type_str[0] == 'u':
                self.code = URFPArgType.URFP_UNSIGNED

            if self.code in (URFPArgType.URFP_INT, URFPArgType.URFP_UNSIGNED):
                if type_str[1:] == '16':
                    self.code = self.code | 0x01
                elif type_str[1:] == '32':
                    self.code = self.code | 0x02
                elif type_str[1:] == '64':
                    self.code = self.code | 0x03

        else: # no type_str
            if type_byte is None:
                raise TypeError('Either type_byte or type_str must be specified')

            self.code = type_byte
            if type_byte == 0x41 and length==0:
                raise TypeError('A nonzero length must be given for arrays')

    def __str__(self):
        if self.code == URFPArgType.URFP_STRUCT_START:
            t = self.name + ':' + '{'
        elif self.code == URFPArgType.URFP_ARRAY_START:
            if self.length != -1:
                t = f'{self.name}:[{self.length}'
            else:
                t = f'{self.name}:['
        elif self.code == URFPArgType.URFP_ARRAY_END:
            t = ']'
        elif self.code == URFPArgType.URFP_STRUCT_END:
            t = '}'
        elif self.code == URFPArgType.URFP_BOOL:
            t = self.name + ':' + 'bool'
        elif self.code == URFPArgType.URFP_STRING:
            t = self.name + ':' + 'string'
        elif URFPArgType.URFP_INT <= self.code < URFPArgType.URFP_INT+0x08: # some int
            width = 8 << (self.code & 3)
            if self.code < URFPArgType.URFP_UNSIGNED:
                t = f'{self.name}:i{width}'
            else: # URFP_UNSIGNED
                t = f'{self.name}:u{width}'
        elif self.code == URFPArgType.URFP_FLOAT:
            t = f'{self.name}:float'
        elif self.code == URFPArgType.URFP_DOUBLE:
            t = f'{self.name}:double'
        elif self.code == URFPArgType.URFP_TYPE_INFO:
            t = f'{self.name}:<type info>'
        else:
            t = f'{self.name}:<unknown({int(self.code)})>'
        return t
