""" Client for URFP HTTP servers """
# pip install requests
# http://docs.python-requests.org/de/latest/user/quickstart.html
import json
import logging
from collections import OrderedDict
from json.decoder import JSONDecodeError
import requests
from urfp.core import URFPException

class URFPHttpConnector:
    """ An instance of this class sends the HTTP requests to the URFP server,
    basically checks the result and returns the 'payload' of the result. """

    def __init__(self, address="10.0.10.76", session=None):
        """Constructor. The constructor might be called without an argument
        or with a single argument. If no argument is given, a default server
        address is assumed ('http://10.0.10.76/urfp/'). A given argument is
        used to set the server address explicitely. If you want a persistent
        connection, provide a requests.session() as the session argument.
        """
        self._logger = logging.getLogger(__name__)
        if session is None:
            self._session = requests.session()
        else:
            self._session = session
        if address[-1] == "/":
            address = address[:-1]
        self.base_address = "http://" + address + "/urfp/"

    def check_error(self, response):
        """ This method checks the response of the URFP server for for an
        error. If an error code (!=0) is given, the method raises an
        exception. Otherwise, it returns the JSON structure without
        error_code and removed error_text."""
        if response["error_code"] == 0:
            response_without_error_info = {}
            for k in response:
                if k == "error_code":
                    continue
                if k == "error_text":
                    continue
                response_without_error_info[k] = response[k]
            return response_without_error_info
        raise URFPException(response["error_code"], response["error_text"])

    def _post_log(self, request_string, arguments, http_response):
        url = ""
        query_string = ""
        http_status = ""
        http_content = ""
        if request_string:
            url = request_string
            self._logger.debug('url: %s', url)
        if arguments:
            if len(arguments) > 0:
                query_string = str(arguments)
                self._logger.debug('query: %s', query_string)
        if http_response:
            self._logger.debug('status: %d %s', http_response.status_code, http_response.reason)
            if http_response.text:
                self._logger.debug('content: %s', http_response.text)

    def perform_request(self, cmd, args, argtype=None, resulttype=None): #pylint: disable=unused-argument
        """ This method performs the request. 'cmd' is the command without the tag.
        The tag (numeric index) is added by this method. """
        tagged_cmd = cmd + "0"
        request_string = self.base_address + tagged_cmd

        # Build querystring from dict
        # TBD: Verify args against argtype specification?
        params = OrderedDict()
        for k,v in args.items():
            if hasattr(v, '__len__') and (not isinstance(v, str)):
                params[k] = ','.join([str(s) for s in v])
            elif isinstance(v, bool):
                if v:
                    params[k] = 'true'
                else:
                    params[k] = 'false'
            elif isinstance(v, str):
                params[k] = v
            else:
                params[k] = str(v)

        http_response = self._session.get(request_string, params = params)

        self._post_log(request_string, args, http_response)
        if http_response.status_code != 200:
            raise URFPException(- http_response.status_code, f'HTTP {http_response.status_code} {http_response.reason}')

        r_base = json.loads(http_response.text)
        # TBD: Verify/format result according to resulttype specification?
        r = r_base[tagged_cmd]
        return self.check_error(r)

    def perform_list_request(self, cmd, listname, args, argtype=None): #pylint: disable=unused-argument
        """ retrieve array[l of (i32,string) pairs AKA (id,name) """
        return self.perform_request(cmd, args)

    async def async_list_request(self, cmd, listname, args, argtype=None): #pylint: disable=unused-argument

        """ retrieve array[l of (i32,string) pairs AKA (id,name) (async version) """
        return self.perform_request(cmd, args)

    def perform_help_request(self, elemtype, elemid):
        """ retrieve details about element """
        json_response = self.perform_request('h'+elemtype, {'id':elemid})
        help_dict = { 'id': elemid, 'desc': json_response['desc'], 'type': {}, 'args': {} }
        for k,v in json_response.items():
            if k == '_args':
                help_dict['args'][k] = v
            if k != 'desc':
                help_dict['type'][k] = v
        return help_dict

    def write_blob(self, elem_id, data):
        """ Upload blob to server using POST request """
        tagged_cmd = "wb0"

        params = {'id': elem_id}
        request_string = self.base_address + tagged_cmd
        http_response = self._session.post(request_string, params = params, data = data)

        self._post_log(request_string, params, http_response)
        r_base = json.loads(http_response.text)
        r = r_base[tagged_cmd]
        return self.check_error(r)
