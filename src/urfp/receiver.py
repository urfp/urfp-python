""" Template for receiving from URFP data sources """
import asyncio
import socket

class URFPSocketReceiverPacket:
    """ Handler for single URFP packet """
    def __init__(self):
        pass

    def decode_packet(self, data):
        """ Decode a packet """

    def decode_stream(self, data):
        """ Extract a packet from stream, then decode it """
        self.decode_packet(data)


class URFPSocketReceiverProtocol:
    """ asyncio.Protocol implementation receiving URFP data """
    def __init__(self, packet: URFPSocketReceiverPacket):
        self.packet = packet
        self.transport = None

    def connection_made(self, transport):
        """ connection_made callback """
        self.transport = transport

    def error_received(self, exc):
        """ error_received callback """
        print(f'Error received: {exc}')

    def connection_lost(self, exc):
        """ connection close callback """
        print(f'Connection closed: {exc}')

    def datagram_received(self, data, addr): #pylint: disable=unused-argument
        """ Decode packet with scan data header at the beginning and data to the end. """
        self.packet.decode_packet(data)

    def data_received(self, data):
        """ Decode data from stream that doesn't necessarily start with scan data header """
        self.packet.decode_stream(data)


class URFPSocketReceiver:
    """ A class that shows how to instantiate a receiver e.g. for UDP Tinypacket output.
    """

    def __init__(self, use_tcp = False, packets: URFPSocketReceiverPacket = None):

        self.port = 0
        self._use_tcp = use_tcp
        self._packets = packets
        self.transport = None
        self.protocol = None

    # --------------- start, receive and shutdown coroutines ---------------
    # needs asyncio

    async def async_start_tcp(self, remote_address='127.0.0.1', port=80, **kwargs):
        """ Connects to target at *remote* address,port """

        loop = asyncio.get_running_loop()

        self.transport, self.protocol = await loop.create_connection(
            lambda: URFPSocketReceiverProtocol(self._packets), remote_address, port, **kwargs)

    async def async_start_udp(self, address=socket.INADDR_ANY, port=0, **kwargs): #pylint: disable=unused-argument
        """ Starts listener for incoming UDP on *local* address,port """

        loop = asyncio.get_running_loop()

        self.transport, self.protocol = await loop.create_datagram_endpoint(
            lambda: URFPSocketReceiverProtocol(self._packets), local_addr=(address,port))

        if self.port == 0:
            receiver = self.transport.get_extra_info('socket')
            actual_address = receiver.getsockname()
            self.port = actual_address[1]

    async def async_start(self, use_tcp=False, **kwargs):
        """ Starts UDP or TCP """

        if use_tcp:
            await self.async_start_tcp(**kwargs)
        else:
            await self.async_start_udp(**kwargs)

    async def async_stop(self):
        """ Stop UDP or TCP """

        self.transport.close()


    async def async_run_for_seconds(self, seconds, **kwargs):
        """ Example usage of async_start_tcp/udp low-level comm """

        await self.async_start(**kwargs)

        await asyncio.sleep(seconds)

        await self.async_stop()


    # --------------- blocking start+receive+shutdown ---------------
    # needs socket

    def run_until(self, condition, **kwargs):
        """ Alternative use of this class without asyncio, relying on sockets and
        callbacks instead: It runs while the condition.check() provided by you
        returns False. It is checked whenever some new data was processed or
        or a one-second timeout passed. The check() shouldn't block but return
        immediately! For handling the new data a callback is used anyway, an
        instance of URFPSocketReceiverPacket that you should provide.
        """
        try:
            if self._use_tcp:
                receiver = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                receiver.connect(**kwargs)

            else:
                receiver = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                receiver.bind(**kwargs)

                if self.port == 0:
                    address = receiver.getsockname()
                    self.port = address[1]

            receiver.settimeout(1)

            count = 0
            while not condition.check():
                try:
                    if self._use_tcp:
                        data = receiver.recv(2048)
                        count = count + len(data)
                        self._packets.decode_stream(data)
                    else:
                        data, = receiver.recvfrom(2048)
                        self._packets.decode_packet(data)

                except socket.timeout:
                    print('Timeout waiting for data from peer')

        finally:
            receiver.close()
