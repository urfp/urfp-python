""" URFP client core functionality, independent of transport """

# pip install requests
# http://docs.python-requests.org/de/latest/user/quickstart.html

import json # für urfp_store_api_details
import logging
from collections import OrderedDict
from urfp.argtype import URFPArgType

class URFPException(Exception):
    """This exception is thrown, if the remote URFP server reports an error.
    """
    def __init__(self, c, m):
        """Constructor."""
        self.code = c
        self.message = m

class URFPOobHandler:
    """ Class for receiving OOB (out of band) data from URFP server.
        Callback input() is called whenever data comes in.
        Note: oob_input() is still supported but deprecated.
    """
    def __init__(self, con=None):
        self._connection = con
        self._logger = logging.getLogger(__name__)

    def input(self, source_id, first_index, data):
        """ Callback input() is called whenever data comes in.
        The "data" argument to input() content depends on the connection.
        Call the connection's source_data_as_dict() method (or derivatives)
        to get it into a connection-independent format, e.g. a Python dict. """

        if self._connection is not None:
            source_data = self._connection.source_data_as_dict(source_id, data)
            self._logger.info('OOB Data ID=%d first=%d %s',
                source_id, first_index, str(source_data))
        else:
            self._logger.info('OOB Data ID=%d first=%d %s',
                source_id, first_index, str(data))

class URFPLookupDictionary:
    """ This class handles a dictionary for a type of elements (variables, functions
    or data sources). When information about an element is requested that is not yet
    in the dictionary, a request is made to gather the missing bits. """

    def __init__(self, con, elemtype, fltr):
        self.connection = con
        self._logger = logging.getLogger(__name__ + '_' + elemtype)
        self.elemtype = elemtype # X: v,f,s,b
        self.listname = fltr # name of the array expected as result of lX command
        self.directory = {}
        self.elemname = {}

    def _update_dictionary(self, prefix=None):
        """ This method requests the list of elements from the URFP server
        and uses the result to update the local lookup-table."""
        parameters = { }
        if prefix:
            parameters = {'pattern': str(prefix) }
        r = self.connection.perform_list_request('l'+self.elemtype, self.listname, parameters)
        if r is not None:
            for elem in r[self.listname]:
                self.directory[elem['name']] = {'id': elem['id']}

    async def _async_update_dictionary(self, prefix=None):
        """ This method requests the list of elements from the URFP server
        and uses the result to update the local lookup-table."""
        parameters = { }
        if prefix:
            parameters = {'pattern': str(prefix) }
        r = await self.connection.async_list_request('l'+self.elemtype, self.listname, parameters)
        if r is not None:
            for elem in r[self.listname]:
                self.directory[elem['name']] = {'id': elem['id']}

    def list_names(self, prefix=None):
        """ Return a list with  names of all elements in this dictionary """
        self._update_dictionary(prefix)
        if prefix:
            return [name for name in list(self.directory.keys()) if name.startswith(prefix)]
        return list(self.directory.keys())

    async def async_list_names(self, prefix=None):
        """ Return a list with  names of all elements in this dictionary (async version) """
        await self._async_update_dictionary(prefix)
        if prefix:
            return [name for name in list(self.directory.keys()) if name.startswith(prefix)]
        return list(self.directory.keys())

    def get_id_by_name(self, name):
        """ Get ID for named element. If not known yet, retrieve it from server. """
        if name not in self.directory:
            self._update_dictionary(name)
        if name not in self.directory:
            raise AttributeError()
        return self.directory[name]['id']

    async def async_get_id_by_name(self, name):
        """ Get ID for named element. If not known yet, retrieve from server (async version) """
        if name not in self.directory:
            await self._async_update_dictionary(name)
        if name not in self.directory:
            raise AttributeError()
        return self.directory[name]['id']

    def _update_help(self, elemname):
        """ Fetch description and type information from server for named element """
        elemid = self.get_id_by_name(elemname)
        self._logger.debug('--- request help for id=%d ---', elemid)
        # The perform_help_request returns a dict with desc, args and (result) type
        self.directory[elemname] = self.connection.perform_help_request(self.elemtype, elemid)

    async def _async_update_help(self, elemname):
        """ Fetch description and type information from server (async version) """
        elemid = await self.async_get_id_by_name(elemname)
        self._logger.debug('--- request help for id=%d ---', elemid)
        # The perform_help_request returns a dict with desc, args and (result) type
        self.directory[elemname] = await self.connection.async_help_request(self.elemtype, elemid)

    def get_attrib_by_name(self, attrib, name):
        """ Common code to update and return type or description for an element """
        if name not in self.directory:
            self._update_dictionary(name)
        if name not in self.directory:
            raise AttributeError()
        if attrib not in self.directory[name]:
            self._update_help(name)
        if attrib not in self.directory[name]:
            raise AttributeError()
        return self.directory[name][attrib]

    async def async_get_attrib_by_name(self, attrib, name):
        """ Common code to update and return type or description for an element (async version) """
        if name not in self.directory:
            await self._async_update_dictionary(name)
        if attrib not in self.directory[name]:
            await self._async_update_help(name)
        if attrib not in self.directory[name]:
            raise AttributeError()
        return self.directory[name][attrib]

    def get_desc_by_name(self, name):
        """ Returns element description, eventually asking server for it if necessary """
        return self.get_attrib_by_name('desc', name)

    def get_type_by_name(self, name):
        """ Returns element type, eventually asking server for it if necessary """
        return self.get_attrib_by_name('type', name)

    def get_args_by_name(self, name):
        """ Returns function argument type, eventually asking server for it if necessary """
        return self.get_attrib_by_name('args', name)

    async def async_get_desc_by_name(self, name):
        """ Returns element description, eventually asking server (async version) """
        return await self.async_get_attrib_by_name('desc', name)

    async def async_get_type_by_name(self, name):
        """ Returns element type, eventually asking server for it (async version) """
        return await self.async_get_attrib_by_name('type', name)

    async def async_get_args_by_name(self, name):
        """ Returns function argument type, eventually asking server for it (async version) """
        return await self.async_get_attrib_by_name('args', name)

    def clear_api_details(self):
        """ Work-In-Progress: load/store elements locally, not by asking server.
            This is currently completely missing the type information! """
        self.directory = {}
        self.elemname = {}

    def set_api_details(self, details):
        """ Work-In-Progress: load/store elements locally, not by asking server.
            This is currently completely missing the type information! """
        for pair in details:
            var_id = pair["id"]
            var_name = pair["name"]
            self.directory[var_name] = var_id

    def get_api_details(self, collection, elemtype):
        """ Work-In-Progress: load/store elements locally, not by asking server.
            This is currently completely missing the type information! """
        elements = {}
        for pair in self.directory:
            elements[pair[0]] = pair[1]
        collection[elemtype] = elements

class URFP:
    """ An instance of this class provides central element of the URFP module.
    It provides basic URFP functionality such as setting and getting values of
    variables. The instance keeps track of the elements (variables, function
    etc.) that are provided by the URFP server.
    """

    def __init__(self, connection):
        # Needs an urfp_http.URFPHttpConnector or \
        #    urfp_tinypacket.URFPTinypacketFrame instance as "connection"!
        self.connection = connection
        async_fn = getattr(connection, "async_request", None)
        self._can_do_async = async_fn is not None and callable(async_fn)
        self._logger = logging.getLogger(__name__)
        self.variable_dictionary = URFPLookupDictionary(connection, "v", "variables")
        self.function_dictionary = URFPLookupDictionary(connection, "f", "functions")
        self.source_dictionary = URFPLookupDictionary(connection, "s", "sources")
        self.blob_dictionary = URFPLookupDictionary(connection, "b", "blobs")

    def list_variables(self, prefix=None):
        """ Return list with names of all known variables """
        return self.variable_dictionary.list_names(prefix)

    def list_functions(self, prefix=None):
        """ Return list with names of all known functions """
        return self.function_dictionary.list_names(prefix)

    def list_sources(self, prefix=None):
        """ Return list with names of all known sources """
        return self.source_dictionary.list_names(prefix)

    def list_blobs(self, prefix=None):
        """ Return list with names of all known blobs """
        return self.blob_dictionary.list_names(prefix)

    async def async_list_variables(self, prefix=None):
        """ Return list with names of all known variables (async version) """
        return await self.variable_dictionary.async_list_names(prefix)

    async def async_list_functions(self, prefix=None):
        """ Return list with names of all known functions (async version) """
        return await self.function_dictionary.async_list_names(prefix)

    async def async_list_sources(self, prefix=None):
        """ Return list with names of all known sources (async version) """
        return await self.source_dictionary.async_list_names(prefix)

    async def async_list_blobs(self, prefix=None):
        """ Return list with names of all known blobs (async version) """
        return await self.blob_dictionary.async_list_names(prefix)

    def _prepare_get_variable(self):

        argtype = [
            URFPArgType('args', -1, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('id'  ,  0, type_byte = URFPArgType.URFP_INT32),
            URFPArgType(''    ,  0, type_byte = URFPArgType.URFP_STRUCT_END) ]

        return argtype

    def get_variable(self, name):
        """ This method requests the URFP server to return the value of
        a certain variable."""

        parameter = {"id": self.variable_dictionary.get_id_by_name(name)}
        argtype = self._prepare_get_variable()
        resulttype = self.variable_dictionary.get_type_by_name(name)
        retval = self.connection.perform_request("gv", \
            parameter, argtype = argtype,  resulttype = resulttype)
        return retval[name]

    async def async_get_variable(self, name):
        """ This method requests the URFP server to return the value of
        a certain variable."""
        if not self._can_do_async:
            return self.get_variable(name)

        parameter = {"id": await self.variable_dictionary.async_get_id_by_name(name)}
        argtype = self._prepare_get_variable()
        resulttype = await self.variable_dictionary.async_get_type_by_name(name)
        retval = await self.connection.async_request("gv", \
            parameter, argtype = argtype, resulttype = resulttype)
        return retval[name]

    def set_variable(self, name, value):
        """ Set variable to given value. """
        parameter = {'id': self.variable_dictionary.get_id_by_name(name), name: value}
        argtype = [
            URFPArgType('args',    0, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('id',      0, type_byte = URFPArgType.URFP_INT32)]
        argtype.extend(self.variable_dictionary.get_type_by_name(name))
        argtype.append(URFPArgType('', 0, type_byte = URFPArgType.URFP_STRUCT_END))
        return self.connection.perform_request("sv", parameter, argtype = argtype)

    async def async_set_variable(self, name, value):
        """ Set variable to given value (async version) """
        if not self._can_do_async:
            return self.set_variable(name, value)

        parameter = {'id': await self.variable_dictionary.async_get_id_by_name(name), name: value}
        argtype = [
            URFPArgType('args',    0, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('id',      0, type_byte = URFPArgType.URFP_INT32)]
        argtype.extend(await self.variable_dictionary.async_get_type_by_name(name))
        argtype.append(URFPArgType('', 0, type_byte = URFPArgType.URFP_STRUCT_END))
        return await self.connection.async_request("sv", parameter, argtype = argtype)

    def call_function(self, name, args = None):
        """ Call a function, given a OrderedDict with arguments """
        parameter = OrderedDict({'id': self.function_dictionary.get_id_by_name(name)})
        if args:
            parameter.update(args)

        argtype = [
            URFPArgType('args',    0, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('id',      0, type_byte = URFPArgType.URFP_INT32)]
        argtype.extend(self.function_dictionary.get_args_by_name(name))
        argtype.append(URFPArgType('', 0, type_byte = URFPArgType.URFP_STRUCT_END))

        resulttype = self.function_dictionary.get_type_by_name(name)
        return self.connection.perform_request("cf", \
            parameter, argtype = argtype, resulttype = resulttype)

    async def async_call_function(self, name, args = None):
        """ Call a function, given a OrderedDict with arguments (async version) """
        if not self._can_do_async:
            return self.call_function(name, args)

        parameter = OrderedDict({'id': await self.function_dictionary.get_id_by_name(name)})
        if args:
            parameter.update(args)

        argtype = [
            URFPArgType('args',    0, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('id',      0, type_byte = URFPArgType.URFP_INT32)]
        argtype.extend(await self.function_dictionary.async_get_args_by_name(name))
        argtype.append(URFPArgType('', 0, type_byte = URFPArgType.URFP_STRUCT_END))

        resulttype = await self.function_dictionary.async_get_type_by_name(name)
        return await self.connection.async_request("cf", \
            parameter, argtype = argtype, resulttype = resulttype)

    def _prepare_enable_sources(self, enable):
        """ Common code for enabling output of a source """
        if enable:
            cmd = 'es'
        else:
            cmd = 'ds'

        argtype = [
            URFPArgType('args',    0, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('sources',-1, type_byte = URFPArgType.URFP_ARRAY_START),
            URFPArgType('id',      0, type_byte = URFPArgType.URFP_INT32),
            URFPArgType('',        0, type_byte = URFPArgType.URFP_ARRAY_END),
            URFPArgType('',        0, type_byte = URFPArgType.URFP_STRUCT_END) ]
        resulttype = [
            URFPArgType('sources',-1, type_byte = URFPArgType.URFP_ARRAY_START),
            URFPArgType('id',      0, type_byte = URFPArgType.URFP_INT32),
            URFPArgType('',        0, type_byte = URFPArgType.URFP_ARRAY_END) ]

        return cmd, argtype, resulttype

    def enable_sources(self, names, enable = True):
        """ Enable output of the sources named in the list """
        parameter = {'sources': []}
        for name in names:
            parameter["sources"].append(self.source_dictionary.get_id_by_name(name))
        cmd, argtype, resulttype = self._prepare_enable_sources(enable)
        return self.connection.perform_request(cmd, \
            parameter, argtype = argtype, resulttype = resulttype)

    async def async_enable_sources(self, names, enable = True):
        """ Enable output of the sources named in the list (async version) """
        if not self._can_do_async:
            return self.enable_sources(names, enable)

        parameter = {'sources': []}
        for name in names:
            parameter["sources"].append(await self.source_dictionary.async_get_id_by_name(name))

        cmd, argtype, resulttype = self._prepare_enable_sources(enable)
        return await self.connection.async_request(cmd, \
            parameter, argtype = argtype, resulttype = resulttype)

    def subscribe(self, names, handler=None):
        """ Subscribe to the sources named in the list, optionally given a special handler """
        if handler is None:
            handler = URFPOobHandler()
        for name in names:
            resulttype=self.source_dictionary.get_type_by_name(name)
            source_id=self.source_dictionary.get_id_by_name(name)
            self.connection.subscribe(source_id, resulttype, handler)
        self.enable_sources(names)

    async def async_subscribe(self, names, handler=None):
        """ Subscribe to the sources named in the list, optionally given a special handler """
        if not self._can_do_async:
            return self.subscribe(names, handler)
        if handler is None:
            handler = URFPOobHandler()
        for name in names:
            resulttype=await self.source_dictionary.async_get_type_by_name(name)
            source_id=await self.source_dictionary.async_get_id_by_name(name)
            self.connection.subscribe(source_id, resulttype, handler)
        return await self.async_enable_sources(names)

    def unsubscribe(self, names):
        """ Unsubscribe from the sources named in the list """
        for name in names:
            source_id=self.source_dictionary.get_id_by_name(name)
            self.connection.unsubscribe(source_id)
        self.enable_sources(names, enable=False)

    async def async_unsubscribe(self, names):
        """ Unsubscribe from the sources named in the list """
        if not self._can_do_async:
            return self.unsubscribe(names)
        for name in names:
            source_id=self.source_dictionary.get_id_by_name(name)
            self.connection.unsubscribe(source_id)
        return await self.async_enable_sources(names, enable=False)

    def _get_attrib_by_name(self, elemtype, attrib, name):
        """ Helper to pick type or desc from dictionary for given elemtype """
        if elemtype == 'v':
            help_dict = self.variable_dictionary.get_attrib_by_name(attrib, name)
        if elemtype == 'f':
            help_dict = self.function_dictionary.get_attrib_by_name(attrib, name)
        elif elemtype == 's':
            help_dict = self.source_dictionary.get_attrib_by_name(attrib, name)
        elif elemtype == 'b':
            help_dict = self.blob_dictionary.get_attrib_by_name(attrib, name)
        else:
            raise NotImplementedError
        return help_dict

    def get_type_by_name(self, elemtype, elemname):
        """ Get type of the specified element with given type v/f/s/b """
        return self._get_attrib_by_name(elemtype, 'type', elemname)

    def get_desc_by_name(self, elemtype, elemname):
        """ Get description of the specified element with given type v/f/s/b  """
        return self._get_attrib_by_name(elemtype, 'desc', elemname)

    async def _async_get_attrib_by_name(self, elemtype, attrib, name):
        """ Helper to pick type or desc from dictionary for given elemtype (async version) """
        if elemtype == 'v':
            help_dict = await self.variable_dictionary.async_get_attrib_by_name(attrib, name)
        if elemtype == 'f':
            help_dict = await self.function_dictionary.async_get_attrib_by_name(attrib, name)
        elif elemtype == 's':
            help_dict = await self.source_dictionary.async_get_attrib_by_name(attrib, name)
        elif elemtype == 'b':
            help_dict = await self.blob_dictionary.async_get_attrib_by_name(attrib, name)
        else:
            raise NotImplementedError
        return help_dict

    async def async_get_type_by_name(self, elemtype, elemname):
        """ Get type of the specified element with given type v/f/s/b (async version) """
        return await self._async_get_attrib_by_name(elemtype, 'type', elemname)

    async def async_get_desc_by_name(self, elemtype, elemname):
        """ Get description of the specified element with given type v/f/s/b (async version) """
        return await self._async_get_attrib_by_name(elemtype, 'desc', elemname)

    def write_blob(self, name, file):
        """ This expects an OPEN file from which the data for the blob is read until EOF """
        elem_id = self.blob_dictionary.get_id_by_name(name)
        if isinstance(file, str):
            with open(file, 'rb') as iofile:
                self.connection.write_blob(elem_id, iofile)
        else: # expect an OPEN IOBase from which t he data for the blob is read until EOF
            self.connection.write_blob(elem_id, file)

    async def async_write_blob(self, name, file):
        """ This expects an OPEN file from which the data is read until EOF (async version) """
        elem_id = await self.blob_dictionary.async_get_id_by_name(name)
        if isinstance(file, str):
            with open(file, 'rb') as iofile:
                await self.connection.async_write_blob(elem_id, iofile)
        else: # expect an OPEN IOBase from which t he data for the blob is read until EOF
            await self.connection.async_write_blob(elem_id, file)

    def read_blob(self, name, *args):
        """ Read binary data """
        raise NotImplementedError

    def set_api_details(self, api_details):
        """ Work-In-Progress: load/store elements locally, not by asking server.
            This is currently completely missing the type information! """
        self.variable_dictionary.set_api_details(api_details)
        self.function_dictionary.set_api_details(api_details)
        self.source_dictionary.set_api_details(api_details)
        self.blob_dictionary.set_api_details(api_details)

    def get_api_details(self):
        """ Work-In-Progress: load/store elements locally, not by asking server.
            This is currently completely missing the type information! """
        collection = {}
        self.variable_dictionary.get_api_details(collection, "variables")
        self.function_dictionary.get_api_details(collection, "functions")
        self.source_dictionary.get_api_details(collection, "sources")
        self.blob_dictionary.get_api_details(collection, "blobs")
        return collection

    def _noattranymore(self):
        raise NotImplementedError("Accessing URFPCore by attribute is not possible anymore! " \
                "Please use get/set_variable, call_function etc.")

    @property
    def variables(self):  #pylint: disable=missing-function-docstring
        self._noattranymore()

    @variables.setter
    def variables(self, v): #pylint: disable=unused-argument,missing-function-docstring
        self._noattranymore()

    @property
    def functions(self): #pylint: disable=missing-function-docstring
        self._noattranymore()

    @functions.setter
    def functions(self, v):  #pylint: disable=unused-argument,missing-function-docstring
        self._noattranymore()

    @property
    def sources(self):  #pylint: disable=missing-function-docstring
        self._noattranymore()

    @sources.setter
    def sources(self, v):  #pylint: disable=unused-argument,missing-function-docstring
        self._noattranymore()

    @property
    def blobs(self):  #pylint: disable=missing-function-docstring
        self._noattranymore()

    @blobs.setter
    def blobs(self, v):  #pylint: disable=unused-argument,missing-function-docstring
        self._noattranymore()

    # TODO: Update the following methods to store/load data types etc not just ID
    def set_api_details_from_file(self, file_name):
        """ Load elements from file (see urfp_store_api_details())
            so the server doesn't have to be asked for help.
            Work-In-Progress: load/store elements locally, not by asking server.
            This is currently completely missing the type information! """
        api_text = ""
        with open(file_name, "r", encoding="utf-8") as file:
            line = file.readline()
            while line:
                api_text = api_text + line
                line = file.readline()
        api_json = json.loads(api_text)
        self.set_api_details_from_dict(api_json)

    def set_api_details_from_dict(self, d):
        """ Load elements from dict (see urfp_get_api_details())
            so the server doesn't have to be asked for help.
            Work-In-Progress: load/store elements locally, not by asking server.
            This is currently completely missing the type information! """
        self.set_api_details(d)
