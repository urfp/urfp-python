# URFP client Python module

## Quick install

Not yet available packaged, work in progress..

```
# sudo apt install python3-build
rm -f dist/*.whl ; python3 -m build ; pip3 install dist/*.whl
```

## Init (HTTP)

```
from urfp.core import URFP
from urfp.http import URFPHttpConnector

def log_requests(url, parameter, http_response, text_response):
    print("URL:                 " + url)
    print("Query:               " + parameter)
    print("HTTP-Response Code:  " + http_response)
    print("Received Content:    " + text_response)

import requests
s = requests.session()
c = urfp.http.URFPHttpConnector('http://server.com', session=s, logger=log_requests)
```

or just (with one HTTP/1.0 session per request and no request logging)...

```
from urfp.core import URFP
from urfp.http import URFPHttpConnector

c = urfp.http.URFPHttpConnector('http://server.com')
```

## Init (Tinypacket/UDP)

```
from urfp.core import URFP
from urfp.tinypacket import URFPTinypacketframe

c = URFPTinypacketFrame(server="10.0.10.62:6272")
```


## Usage after Init

The elements named as listed by lv, lf etc. become attributes:

```
u = URFP(c)
u.set_variable("something_enabled", True)
print(u.get_variable("something_enabled")

res = u.call_function("arbitrary_func", {'argument': 123, 'second':'value'})
```


