#
# Note: The tests rely (in parts) on a connected device.
#

import unittest
from time import sleep
from urfp import URFP
from urfp import URFPCore
from urfp import URFPFunctions
from urfp import URFPException
class TestURFPUrlHandling(unittest.TestCase):
	def test_default_url(self):
		u = URFP()
		self.assertEqual("http://10.0.10.76/urfp/", u.base_access.connector.base_address)
		
	def test_base_url_1(self):
		u = URFP("http://10.0.10.76")
		self.assertEqual("http://10.0.10.76/urfp/", u.base_access.connector.base_address)

	def test_base_url_2(self):
		u = URFP("http://www.telos.de")
		self.assertEqual("http://www.telos.de/urfp/", u.base_access.connector.base_address)

	def test_base_url_3(self):
		u = URFP("http://www.telos.de/")
		self.assertEqual("http://www.telos.de/urfp/", u.base_access.connector.base_address)

class TestURFPAccessRights(unittest.TestCase):
	def test_change_vars(self):
		u = URFP()
		try:
			u.variables = 5
		except AttributeError:
			return
		self.fail("An exception has been expected.")

class TestURFPExamination(unittest.TestCase):

	def test_exception_on_missing_variable(self):
		u = URFP()
		v = u.variables
		try:
			print(v.xxx)
		except AttributeError:
			return
		self.fail("An exception has been expected.")

# Getting Variables
# - Integer
# - String
# - Arrays
# Setting Variables
# - Integer
# - String
# - Arrays
class TestURFPVariablesGet(unittest.TestCase):
	def test_vendor(self):
		u = URFP()
		result = u.variables.vendor
		self.assertEqual("", result)
	def test_product(self):
		u = URFP()
		result = u.variables.product
		self.assertEqual("", result)
	def test_dist_slope2(self):
		u = URFP()
		result = u.variables.dist_slope2
		self.assertEqual(62006, result)
	def test_dist_port(self):
		u = URFP()
		result = u.variables.port
		self.assertEqual(6464, result)



class TestURFPExceptions(unittest.TestCase):
	""" This class tests whether the URPF module detects URFP errors and returns
	the result accordingly. """
	def test_get_exception(self):
		u = URFP()
		v = u.variables
		# First, we set measure_start and measure_stop to '0' while avoiding
		# invalid combination(s)
		if ((v.measure_start < 0) and (v.measure_stop < 0)):
			v.measure_stop = 0
			v.measure_start = 0
		else:
			v.measure_start = 0
			v.measure_stop = 0
		# We force an invalid combination: measure_start > measure_stop
		try:
			v.measure_start = v.measure_stop + 1
		except URFPException as ax:
			# Note: The exact number and message depend on the implementation
            # of the URFP source. If the implementation changes, the result
            # might change, also.
			self.assertEqual("Out of range", ax.message)
			self.assertEqual(1, ax.code)
			return
		self.fail("An exception has been expected.")

		

class TestURFPVariableSetGet(unittest.TestCase):
	def setUp(self):
		self.u = URFP()
		self.v = self.u.variables
	def test_get_set_get_pilot(self):
		result = self.v.pilot_enable
		new_value = None
		if "on" == result:
			new_value = "off"
		elif "off" == result:
			new_value = "on"
		else:
			self.fail("The value of the variable pilot was unexpected: '" + result + "'.")
		self.v.pilot_enable = new_value
		result = self.v.pilot_enable
		self.assertEqual(new_value, result)
	
	def test_get_set_layers_1(self):
		self.v.layers_enabled = 0
		self.assertEqual(["off","off","off","off"], self.v.layer_enable)
	
	def test_get_set_layers_2(self):
		self.v.layers_enabled = 15
		self.assertEqual(["on","on","on","on"], self.v.layer_enable)
	
	def test_get_set_layers_3(self):
		self.v.layers_enabled = 7
		self.assertEqual(["on","on","on","off"], self.v.layer_enable)

	def test_get_set_layer_4(self):
		self.v.layers_enabled = 0
		self.v.layer_enable = ["on", "on", "off", "off"]
		self.assertEqual(3, self.v.layers_enabled)
		pass

	def test_get_set_layer_5(self):
		self.v.layers_enabled = 0
		self.v.layer_enable = ["off", "on", "on", "off"]
		self.assertEqual(6, self.v.layers_enabled)
		pass

class TestURFPFunctions(unittest.TestCase):
	def test_get_functions(self):
		u = URFP()
		f = u.functions
		
	def test_reject_functions_assignment(self):
		u = URFP()
		try:
			u.functions = URFPFunctions()
		except AttributeError:
			return
		fail("Exception was expected.")
		
	def test_reject_functions_assignment(self):
		u = URFP()
		f = u.functions
		try:
			u.functions.this_function_doesnt_exist()
		except AttributeError:
			return
		fail("Exception was expected.")
		
	def test_sample_function_get_parameter(self):
		""" This method calls 'get_parameter' and checks, whether some expected
		parameters are in the result set. """
		u = URFP()
		f = u.functions
		r = f.get_parameter()
		self.assertTrue("ip_mode" in r)
		self.assertTrue("scan_direction" in r)
		
	def test_sample_function_list_parameters(self):
		""" This metho calls 'list_parameter' and checks, whether some expected
		parameters are in the result set. """
		u = URFP()
		f = u.functions
		r = f.list_parameters()
		self.assertTrue("ip_mode" in r["parameters"])
		self.assertTrue("scan_direction" in r["parameters"])

		
class TestURFPGetParameter(unittest.TestCase):
	""" This method performs some tests issuing the 'get_parameter()' function
		of the URFP device. The 'get_parameter()' function has been chosen since
		it allows an easy verification whether function arguments are properly
		recogniced by the URFP device."""
	def test_get_parameter_all(self):
		""" Call get_parameter() without an argument. The complete paramter
		list is expected as result. As indication for the correct result, we
		check some expected parameters ("ip_mode" and "scan_direction") and
		we check the total number of returned parameters. """
		u = URFP()
		f = u.functions
		r = f.get_parameter()
		self.assertTrue("ip_mode" in r)
		self.assertTrue("scan_direction" in r)
		self.assertEqual(36, len(r), "Unexpected number of parameters received.")

	def test_get_parameter_ip_mode(self):
		""" This method requests only the parameter 'ip_mode'. The result must
		be a dictionary with a single element. """
		u = URFP()
		f = u.functions
		r = f.get_parameter({"list":"ip_mode"})
		self.assertTrue("ip_mode" in r)
		self.assertEqual(len(r), 1, "Single parameters expected.")

	def test_get_parameter_scan_direction(self):
		""" This method requests only the parameter 'scan_direction'. The result
		must be a dictionary with a single element. """
		u = URFP()
		f = u.functions
		r = f.get_parameter({"list":"scan_direction"})
		self.assertTrue("scan_direction" in r)
		self.assertEqual(len(r), 1, "Single parameters expected.")

	def test_get_parameter_scan_direction_ip_mode(self):
		u = URFP()
		f = u.functions
		r = f.get_parameter({"list":"ip_mode;scan_direction"})
		self.assertTrue("scan_direction" in r)
		self.assertTrue("ip_mode" in r)
		self.assertEqual(len(r), 2)

		
		
if __name__ == "__main__":
    unittest.main()
