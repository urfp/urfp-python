#!/usr/bin/python3

# Script for decoding URFP Tinypacket in live or captured IP data
#
# Record with tcpdump or wireshark (not pcapng), e.g.
#
#   tcpdump -i br0 -s 65535 -w trace.pcap
#
# PCAPNG capture files can be converted using "editcap -F libpcap original.pcapng converted.pcap"
#
# Needs dpkt and pypcap. In Ubuntu this can be installed with
#
#   pip3 install dpkt
#   apt install libpcap0.8-dev
#   pip3 install pypcap # https://github.com/dugsong/pypcap
#
# In Windows see
#
#   https://pypcap.readthedocs.io/en/latest/ (not tested)
#
# (C) 2019-2024 telos Systementwicklung GmbH, K.Waschk
#

import re
import sys
import getopt
import socket
import time
import math
import array
import struct
import binascii

import sys
sys.path.insert(0, '../src')

from urfp.tinypacket import URFPTinypacketCodec


DEFAULT_PORT = 6272
DEFAULT_DEV = '' # set to '' or 'udp' to listen on DEFAULT_PORT, else interface name such as 'enp5s0' or 'br0'

have_dpkt = True
try:
    import dpkt
except ImportError:
    have_dpkt = False

have_pcap = True
try:
    import pcap
except ImportError:
    have_pcap = False

# -v and -q commandline options increase or decrease this
verbose = 0

packet_index = 0


urfp_codec = URFPTinypacketCodec()

# system time (s)
start_timestamp = -1
prev_timestamp = 0
suppress_sources = False

env = { }

message = { }

def update_packet_stats(timestamp):
    global packet_index
    global timestr
    global start_timestamp
    global prev_timestamp

    packet_index = packet_index + 1

    # Output timestamp as delta to time when trace started
    if start_timestamp < 0:
        start_timestamp = timestamp
    #timestamp = timestamp - start_timestamp
    prev_timestamp = timestamp

    timestr = "%s.%06d %.6f" % (
        time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(timestamp)),
        1000000*(timestamp-math.floor(timestamp)),
        timestamp)

    ## Debug: Report "large" deltas to ease hunt for timeouts in trace
    #if prev_timestamp > 0:
    #    if timestamp - prev_timestamp > 0.25:
    #        print("**** %f: big timestamp delta %f" % ts, ts-prev_timestamp)


def inet_to_str(inet):
    """Convert inet object to a string

        Args:
            inet (inet struct): inet network address
        Returns:
            str: Printable/readable IP address
    """
    # First try ipv4 and then ipv6
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)

######## CALLBACK: process a single packet #############################################################

def process_udp_data(timestamp, to_server, udp_data):
    global packet_index
    global timestr
    global start_timestamp
    global prev_timestamp
    global message

    if len(udp_data) < 8:
        if verbose > 1:
            print("%u %s %s Short header: only %d bytes, not even 8" % (packet_index, timestr, to_server, len(udp_data)))
        return

    if verbose > 0:
        pts = timestr
    elif to_server:
        pts = '==>'
    else:
        pts = '<=='

    pno = 0
    offset = 0
    printed_something = False
    while offset+8 <= len(udp_data):

        tinypacket = udp_data[offset:(offset+8)]
        offset = offset + 8

        prefix = (tinypacket[0]>>4) & 15
        length = (tinypacket[0] & 15) + ((tinypacket[1]>>3) & 16)
        tag    = (tinypacket[1] & 127)

        show_code = '--'
        if prefix >= 12:
            code = "%c%c" % (tinypacket[2], tinypacket[3])
            data = tinypacket[4:(4+length)]
            env[tag] = {'code': code, 'pno': 0}
            show_code = code
            pno = 0
        else:
            data = tinypacket[2:(2+length)]
            if tag in env:
                pno = env[tag]['pno'] + 1
                env[tag]['pno'] = pno
                code = env[tag]['code']
            else:
                pno = 999
                code = '--'

        content = None
        updated = urfp_codec.input(tinypacket)
        for tag in updated:
            if updated[tag]:
                if urfp_codec._env[tag].final and urfp_codec._env[tag].code == 'lg':
                    content = urfp_codec.get(tag)
                    source_id, first_index = struct.unpack_from('<II', content)
                    content_length = len(content)-8
                    # print(source_id, first_index, content_length)

        if not suppress_sources or code != 'lg':
            printed_something = True
            print('%s %4u %4u %x %s%c(%d)' % (binascii.hexlify(tinypacket), packet_index, offset, prefix, show_code, tag, length), end='')

            print(' ', binascii.hexlify(data), end='')

            if pno == 0:
                if code == 'lg' and length>=4:
                    print(' id=%u' % struct.unpack_from('<I', data), end='')
            elif pno == 1:
                if code == 'lg' and length>=4:
                    print(' first=%u' % struct.unpack_from('<I', data), end='')

            print()

    if printed_something:
        if offset < len(udp_data):
            print('--------------------------------------------------- rest %d' % (len(udp_data) - offset))
        else:
            print('---------------------------------------------------')

    sys.stdout.flush()

def process_packet(timestamp, buf):
    global packet_index
    global timestr
    global start_timestamp
    global prev_timestamp
    global port

    update_packet_stats(timestamp)

    eth = dpkt.ethernet.Ethernet(buf)
    if isinstance(eth.data, dpkt.arp.ARP):
        if verbose > 1:
            print("%u %s ARP" % (packet_index, timestr))
        return

    elif not isinstance(eth.data, dpkt.ip.IP):
        if verbose > 1:
            print("%u %s no IP" % (packet_index, timestr))
        return

    ip = eth.data
    if isinstance(ip.data, dpkt.icmp.ICMP):
        icmp = ip.data
        if verbose > 1:
            print('%u %s ICMP type:%d code:%d' % (packet_index, timestr, icmp.type, icmp.code))
        return

    elif isinstance(ip.data, dpkt.tcp.TCP):
        tcp = ip.data
        if tcp.dport == 80 or tcp.sport == 80 or tcp.dport == 8080 or tcp.sport == 8080:
            if len(tcp.data) > 0:
                if verbose > 1:
                    print ('%u %s HTTP: %s -> %s' % (packet_index, timestr, inet_to_str(ip.src), inet_to_str(ip.dst)))
                    print ('    Data: %s' % (tcp.data))
        elif tcp.dport == 23 or tcp.sport == 23:
            if len(tcp.data) > 0 and verbose > 1:
                print ('%u %s TELNET: %s -> %s: %s' % (packet_index, timestr, inet_to_str(ip.src), inet_to_str(ip.dst), str(tcp.data)))
        elif verbose > 1:
           print("%u %s no HTTP nor TELNET" % (packet_index, timestr))
        return

    elif not isinstance(ip.data, dpkt.udp.UDP):
        if verbose > 1:
            print("%u %s no TCP nor UDP" % (packet_index, timestr))
        return

    udp = ip.data
    if udp.dport != port and udp.sport != port:
        if verbose > 1:
            print("%u %s UDP to %d, no URFP" % (packet_index, timestr, udp.dport))
        return

        print(packet_index, timestr)

    if udp.dport == port:
        to_server = True
    else:
        to_server = False

    process_udp_data(timestamp, to_server, udp.data)

####### PROCESS PCAP FILE (calling function above for each packet) #####################################

def usage():
    printf('usage: %s [-v (verbose)] [-q (quiet)] [-t runtime(secs)] [-x xfer_out_to_file] [pcap file|-i device]' % sys.argv[0])
    sys.exit(1)

def main():
    global verbose
    global xfer_out_to_file
    global plot_after_reading
    global lines,scan_angle_inc
    global suppress_sources
    global port
    runtime_secs = 0

    opts, args = getopt.getopt(sys.argv[1:], 'i:p:qvst:h')
    port = DEFAULT_PORT
    name = DEFAULT_DEV
    for o, a in opts:
        if o == '-i': name = a
        elif o == '-p': port = int(a)
        elif o == '-q': verbose = verbose - 1
        elif o == '-v': verbose = verbose + 1
        elif o == '-s': suppress_sources = True
        elif o == '-t': runtime_secs = int(a)
        else:
            usage()

    if len(args) > 0:
        if not have_dpkt and not have_pcap:
            print("Missing dpkt and/or pcap module to parse captured traffic")
        else:
            if verbose >= 0:
                print("Read from %s" % args[0])
            f = open(args[0], 'rb')
            pc = dpkt.pcap.Reader(f)
            # pc.setfilter(' '.join(args))
            for ts, buf in pc:
                process_packet(ts, buf)
    else:
        if name == '':
            print("Listening on port %d for UDP data" % port)
            name = 'udp'
        if name == 'udp':
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.bind(('', port))
            while runtime_secs == 0 or time.clock_gettime(time.CLOCK_MONOTONIC) < end_time:
                data, addr = sock.recvfrom(5000)
                # print("received message: %s" % data)
                now = time.clock_gettime(time.CLOCK_REALTIME)
                update_packet_stats(now)
                result = process_udp_data(now, False, data)

        elif not have_dpkt or not have_pcap:
            print("Missing dpkt and/or pcap module to capture live from %s" % name)
        else:
             #print("Capture live from %s" % name)
            if runtime_secs > 0:
                import signal
                signal.alarm(runtime_secs)

            try:
                # timeout_ms kurz zu setzen ist wichtig damit zwischendurch mal nach SIGINT geguckt werden kann
                pc = pcap.pcap(name, immediate=True, timeout_ms=100)
                # pc.setfilter(' '.join(args))
                for ts, buf in pc:
                    process_packet(ts, buf)

            except KeyboardInterrupt:
                print('Interrupted trace')

            if verbose >= 0:
                if len(args) == 0:
                    nrecv, ndrop, nifdrop = pc.stats()
                    print('%d packets received by filter' % nrecv )
                    print('%d packets dropped by kernel' % ndrop)

        h = (prev_timestamp - start_timestamp) // 3600
        m = ((prev_timestamp - start_timestamp) - 3600*h) // 60
        print('%d:%02d hours' % (h, m))

if __name__ == '__main__':
    main()
