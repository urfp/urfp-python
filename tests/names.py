#!/usr/bin/python3
#
# Simple example
#

import time
import asyncio

import sys
sys.path.insert(0, '../src')

from urfp.core import URFP
#from urfp.http import URFPHttpConnector
from urfp.tinypacket import URFPTinypacketFrame

async def async_main():
    c = URFPTinypacketFrame(server="10.0.10.62:6272", local="10.0.10.10:0")
    #c = URFPHttpConnector("10.0.10.62")
    u = URFP(c)

    print("Variables:", u.list_variables())
    #print("Variables:", await u.async_list_variables())
    print("Functions:", u.list_functions())
    print("Sources:", u.list_sources())
    print("Blobs:", u.list_blobs())

    print("Variables Motor:", u.list_variables("motor"))

if __name__ == "__main__":
    asyncio.run(async_main())
