#!/usr/bin/python3

# Script for decoding URFP Tinypacket from Saleae Logic captured serial stream
#
# (C) 2024 telos Systementwicklung GmbH, K.Waschk
#

import re
import sys
import getopt
import time
import math
import array
import struct
import binascii

from crccheck.crc import Crc16Kermit,Crc16Xmodem

#  uint8_t address;
#  uint8_t sequence_and_size;
#  uint8_t data[8];
#  uint8_t crc16_msb;
#  uint8_t crc16_lsb;

import sys
sys.path.insert(0, '../src')

from urfp.tinypacket import URFPTinypacketCodec


# -v and -q commandline options increase or decrease this
verbose = 0

packet_index = 0

urfp_codec = URFPTinypacketCodec()

# system time (s)
start_timestamp = -1
prev_timestamp = 0
suppress_sources = False

env = { }

message = { }

def process_data(timestamp, to_server, udp_data):
    global packet_index
    global timestr
    global start_timestamp
    global prev_timestamp
    global message

    if len(udp_data) < 8:
        if verbose > 1:
            print("%u %s %s Short header: only %d bytes, not even 8" % (packet_index, timestr, to_server, len(udp_data)))
        return

    if verbose > 0:
        pts = timestr
    elif to_server:
        pts = '==>'
    else:
        pts = '<=='

    pno = 0
    offset = 0
    printed_something = False
    while offset+8 <= len(udp_data):

        tinypacket = udp_data[offset:(offset+8)]
        offset = offset + 8

        prefix = (tinypacket[0]>>4) & 15
        length = (tinypacket[0] & 15) + ((tinypacket[1]>>3) & 16)
        tag    = (tinypacket[1] & 127)

        show_code = '--'
        if prefix >= 12:
            code = "%c%c" % (tinypacket[2], tinypacket[3])
            data = tinypacket[4:(4+length)]
            env[tag] = {'code': code, 'pno': 0}
            show_code = code
            pno = 0
        else:
            data = tinypacket[2:(2+length)]
            if tag in env:
                pno = env[tag]['pno'] + 1
                env[tag]['pno'] = pno
                code = env[tag]['code']
            else:
                pno = 999
                code = '--'

        if not suppress_sources or code != 'lg':
            printed_something = True
            print('%s %4u %4u %s %x %s%c(%d)' % (binascii.hexlify(tinypacket), packet_index, offset, pts, prefix, show_code, tag, length), end='')

            print(' ', binascii.hexlify(data), end='')

            if pno == 0:
                if code == 'lg' and length>=4:
                    print(' id=%u' % struct.unpack_from('<I', data), end='')
            elif pno == 1:
                if code == 'lg' and length>=4:
                    print(' first=%u' % struct.unpack_from('<I', data), end='')

            print()

            updated = urfp_codec.input(tinypacket)
            for tag in updated:
                if updated[tag]:
                    if urfp_codec._env[tag].final:
                        print('   ', urfp_codec.get(tag))

    if printed_something:
        if offset < len(udp_data):
            print('--------------------------------------------------- rest %d' % (len(udp_data) - offset))
        else:
            print('---------------------------------------------------')

    sys.stdout.flush()

####### PROCESS PCAP FILE (calling function above for each packet) #####################################

def usage():
    printf('usage: %s [-v (verbose)] [-q (quiet)] [-t runtime(secs)] [-x xfer_out_to_file] [ file]' % sys.argv[0])
    sys.exit(1)

def main():
    global verbose
    global lines
    global suppress_sources

    opts, args = getopt.getopt(sys.argv[1:], 'i:qvst:x:hp')
    for o, a in opts:
        if o == '-q': verbose = verbose - 1
        elif o == '-v': verbose = verbose + 1
        elif o == '-s': suppress_sources = True
        else:
            usage()

    lost = 0
    data = { }
    server_name = None

    with open(args[0], 'r', encoding='utf-8') as saleae_csv:
        header_line = saleae_csv.readline()
        for line in saleae_csv:

            field = line.split(',')
            name = field[0].strip('\"')
            if server_name is None:
                server_name = name # first come first server

            #typestr = field[1].strip('\"')
            start_time = float(field[2])
            duration = float(field[3])
            value = bytearray.fromhex(field[4][2:])
            #error = field[5].strip('\"')

            if not name in data:
                data[name] = value
            elif len(data[name]) < 12:
                data[name] = data[name] + value
            else:
                data[name] = data[name][1:] + value
                lost = lost + 1

            packet = data[name]
            if len(packet) == 12:
                if Crc16Xmodem.calc(packet) == 0:
                    if lost > 0:
                        print(f'======================================================= {lost}!!')
                    address = packet[0]
                    sequence_and_size = packet[1]
                    payload = packet[2:10]
                    crc16 = packet[10]*256 + packet[11]

                    # print(f'    {address:02x} {sequence_and_size:02x} {binascii.hexlify(payload)} {crc16:04x} ({lost})')
                    process_data(0, name == server_name, payload)

                    lost = 0
                    data[name] = b''

if __name__ == '__main__':
    main()
