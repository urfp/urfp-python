#
# Simple example
#

from urfp.core import URFP
from urfp.core import urfp_get_vars
from urfp.core import urfp_get_api_details
from urfp.core import urfp_store_api_details


##
# Diese Funktion wird bei jedem HTTP-Request aufgerufen, der vom URFP Objekt
# ausgeloest wird.
#
def log_requests(url, parameter, http_response, text_response):
	print()
	print("URL:                 " + url)
	print("Query:               " + parameter)
	print("HTTP-Response Code:  " + http_response)
	print("Received Content:    " + text_response)

if True:
    from urfp.console import URFPConsoleChatter
    c = URFPConsoleChatter("/dev/ttyS0", logger=log_requests)
elif True:
    from urfp.http import URFPHttpConnector
    c = URFPHttpConnector("http://10.0.10.62", logger=log_requests)
else:
    from urfp.tinypacket import URFPTinypacketFrame
    c = URFPTinypacketFrame(server="10.0.10.62:6272", logger=log_requests)
    u = URFP(c)
    print(u.variables.fpga_rev)

print("-----------------------------------------------------------------------")
print("Fetch API details beforehand into sample.json:")
print("-----------------------------------------------------------------------")
api_details = urfp_get_api_details(c)
urfp_store_api_details(c, "sample.json")

print("-----------------------------------------------------------------------")
print("Example with pre-set API details:")
print("-----------------------------------------------------------------------")
u = URFP(c)
# u.set_api_details_from_dict(api_details)
u.set_api_details_from_file("sample.json")
u.variables.layers_enabled = 0
u.variables.layers_enabled = 1
print(u.variables.layers_enabled)

print("-----------------------------------------------------------------------")
print("Example without pre-set API details:")
print("-----------------------------------------------------------------------")
u = URFP(c)
u.variables.layers_enabled = 0
u.variables.layers_enabled = 0
print(u.variables.layers_enabled)
