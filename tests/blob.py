#!/usr/bin/python3
#
# Simple example
#

import io
import time
import asyncio
import logging

import sys
sys.path.insert(0, '../src')

from urfp.core import URFP, URFPOobHandler
from urfp.http import URFPHttpConnector
from urfp.tinypacket import URFPTinypacketFrame

async def async_main():
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    if False:
        c = URFPTinypacketFrame(server="10.0.10.62:6272", local="10.0.10.10:0")
    else:
        c = URFPHttpConnector("10.0.10.62")
    u = URFP(c)

    #u.write_blob("update", "zero.bin")

    blob = io.BytesIO(b"test")
    u.write_blob("update", blob)

if __name__ == "__main__":
    asyncio.run(async_main())





