#!/usr/bin/python3
#
# Simple example
#

import time
import asyncio
import logging

import sys
sys.path.insert(0, '../src')

from urfp.core import URFP, URFPOobHandler
from urfp.http import URFPHttpConnector
from urfp.tinypacket import URFPTinypacketFrame

sources = ['adc','line']
#sources = ['line']

class AdcHandler(URFPOobHandler):
    def __init__(self, con, logger):
        self._connector = con
        self._logger = logger
    def input(self, source_id, first_index, data):
        d = self._connector.source_data_as_list_of_dicts(source_id, data)
        self._logger.debug('data src=%d first=%d len=%d' % (source_id, first_index, len(d)))

async def async_main():
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    c = URFPTinypacketFrame(server="10.0.10.62:6272", local="10.0.10.10:0")
    u = URFP(c)

    measure_auto_enabled = False
    if (u.get_variable("servicing") & 64) != 0:
        measure_auto_enabled = True
        u.set_variable("measure_enable", True)

    h = AdcHandler(c, logger)
    u.subscribe(sources, h)

    start_time = time.time()

    await c.async_start_receiver()

    while time.time() < start_time + 10:
        print(time.time())

        await asyncio.sleep(1)

    await c.async_stop_receiver()

    u.unsubscribe(sources)
    if measure_auto_enabled:
        u.set_variable("measure_enable", False)

if __name__ == "__main__":
    asyncio.run(async_main())





