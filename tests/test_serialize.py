#!/usr/bin/python3

import sys
sys.path.insert(0, '../src')

import logging
logging.basicConfig(level=logging.DEBUG)

from urfp.argtype import URFPArgType
from urfp.tinypacket import URFPTinypacketCodec

def test_serialize_basic_types():
    codec = URFPTinypacketCodec()

    utype = [ URFPArgType('somebool', -1, type_byte = URFPArgType.URFP_BOOL) ]
    result = codec.serialize(True, utype)
    assert result == b'\x01'

    utype = [ URFPArgType('somestr', -1, type_byte = URFPArgType.URFP_STRING) ]
    result = codec.serialize("ABC", utype)
    assert result == b'\x41\x42\x43\x00'

    utype = [ URFPArgType('someu8', -1, type_byte = URFPArgType.URFP_UINT8) ]
    result = codec.serialize(0x0d, utype)
    assert result == b'\x0d'

    utype = [ URFPArgType('someu16', -1, type_byte = URFPArgType.URFP_UINT16) ]
    result = codec.serialize(0x1234, utype)
    assert result == b'\x34\x12'

    utype = [ URFPArgType('someu32', -1, type_byte = URFPArgType.URFP_UINT32) ]
    result = codec.serialize(0x12345678, utype)
    assert result == b'\x78\x56\x34\x12'

    utype = [ URFPArgType('someu64', -1, type_byte = URFPArgType.URFP_UINT64) ]
    result = codec.serialize(0xf012345678, utype)
    assert result == b'\x78\x56\x34\x12\xf0\x00\x00\x00'

    utype = [ URFPArgType('somei8', -1, type_byte = URFPArgType.URFP_INT8) ]
    result = codec.serialize(-1, utype)
    assert result == b'\xff'

    utype = [ URFPArgType('somei16', -1, type_byte = URFPArgType.URFP_INT16) ]
    result = codec.serialize(-2, utype)
    assert result == b'\xfe\xff'

    utype = [ URFPArgType('somei32', -1, type_byte = URFPArgType.URFP_INT32) ]
    result = codec.serialize(-3, utype)
    assert result == b'\xfd\xff\xff\xff'

    utype = [ URFPArgType('otheri32', -1, type_byte = URFPArgType.URFP_INT32) ]
    result = codec.serialize(+3, utype)
    assert result == b'\x03\x00\x00\x00'

    utype = [ URFPArgType('somei64', -1, type_byte = URFPArgType.URFP_INT64) ]
    result = codec.serialize(0xf012345678, utype)
    assert result == b'\x78\x56\x34\x12\xf0\x00\x00\x00'

    utype = [ URFPArgType('somefloat', -1, type_byte = URFPArgType.URFP_FLOAT) ]
    result = codec.serialize(12.3456789e3, utype)
    assert result == b'\xb7\xe6\x40\x46'

    utype = [ URFPArgType('somedbl', -1, type_byte = URFPArgType.URFP_DOUBLE) ]
    result = codec.serialize(12.3456789e3, utype)
    assert result == b'\xa1\xf8\x31\xe6\xd6\x1c\xc8\x40'

def test_serialize_basic_array():
    codec = URFPTinypacketCodec()
    utype = [
        URFPArgType('twobools', -1, type_byte = URFPArgType.URFP_ARRAY_START),
        URFPArgType('somebool', -1, type_byte = URFPArgType.URFP_BOOL),
        URFPArgType('', -1, type_byte = URFPArgType.URFP_ARRAY_END)
    ]
    result = codec.serialize([False, True], utype)
    assert result == b'\x00\x01'

def test_serialize_basic_struct():
    codec = URFPTinypacketCodec()
    utype = [
        URFPArgType('somestruct', -1, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('twobools', -1, type_byte = URFPArgType.URFP_ARRAY_START),
                URFPArgType('somebool', -1, type_byte = URFPArgType.URFP_BOOL),
            URFPArgType('', -1, type_byte = URFPArgType.URFP_ARRAY_END),
        URFPArgType('', -1, type_byte = URFPArgType.URFP_STRUCT_END),
    ]
    result = codec.serialize({'twobools':[False, True]}, utype)
    assert result == b'\x00\x01'

def test_serialize_array_of_structs():
    codec = URFPTinypacketCodec()
    utype = [
        URFPArgType('twostructs', -1, type_byte = URFPArgType.URFP_ARRAY_START),
            URFPArgType('somestruct', -1, type_byte = URFPArgType.URFP_STRUCT_START),
                URFPArgType('somebool', -1, type_byte = URFPArgType.URFP_BOOL),
                URFPArgType('somestr', -1, type_byte = URFPArgType.URFP_STRING),
            URFPArgType('', -1, type_byte = URFPArgType.URFP_STRUCT_END),
        URFPArgType('', -1, type_byte = URFPArgType.URFP_ARRAY_END)
    ]
    result = codec.serialize([{"somebool":False, "somestr": "foo"}, {"somebool":True, "somestr": "bar"}], utype)
    assert result == b'\x00foo\x00\x01bar\x00'

def test_serialize_struct_of_arrays():
    codec = URFPTinypacketCodec()
    utype = [
        URFPArgType('somestruct', -1, type_byte = URFPArgType.URFP_STRUCT_START),
            URFPArgType('first', -1, type_byte = URFPArgType.URFP_ARRAY_START),
                URFPArgType('somebool', -1, type_byte = URFPArgType.URFP_BOOL),
            URFPArgType('', -1, type_byte = URFPArgType.URFP_ARRAY_END),
            URFPArgType('second', -1, type_byte = URFPArgType.URFP_ARRAY_START),
                URFPArgType('somebool', -1, type_byte = URFPArgType.URFP_BOOL),
            URFPArgType('', -1, type_byte = URFPArgType.URFP_ARRAY_END),
        URFPArgType('', -1, type_byte = URFPArgType.URFP_STRUCT_END),
    ]
    result = codec.serialize({"first": [False, True, False], "second": [True, False, False]}, utype)
    assert result == b'\x00\x01\x00\x01\x00\x00'

