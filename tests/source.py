#!/usr/bin/python3
#
# Simple example
#

import time
import asyncio

import sys
sys.path.insert(0, '../src')

from urfp.core import URFP
from urfp.tinypacket import URFPTinypacketFrame

#sources = ['motor_test']
#sources = ['motor_diag', 'adc']
sources = ['adc']

async def async_main():
    c = URFPTinypacketFrame(server="10.0.10.62:6272", local="10.0.10.10:0")
    u = URFP(c)

    print(u.variables.fpga_rev)
    print(u.sources.help('motor_test'))

    measure_auto_enabled = False

    enabled = u.subscribe(sources)

    if 'motor_test' in sources:
        u.variables.motor_tsrc_size = 5
        u.variables.motor_tsrc_period = 333
    if 'adc' in sources:
        if u.variables.manf_enabled:
            measure_auto_enabled = True
            u.variables.measure_enable = True

    start_time = time.time()

    await c.async_start_receiver()

    while time.time() < start_time + 2:

        if 'motor_test' in sources:
            print(u.variables.motor_tsrc_size)

        await asyncio.sleep(1)

    await c.async_stop_receiver()

    if 'motor_test' in sources:
        u.variables.motor_tsrc_period = 0
    if 'adc' in sources:
        if measure_auto_enabled:
            u.variables.measure_enable = False

    enabled = u.unsubscribe(sources)

if __name__ == "__main__":
    asyncio.run(async_main())





